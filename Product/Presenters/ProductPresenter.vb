﻿Imports ResturantBillingSystem
Imports System.Data.SqlClient

Public Class ProductPresenter
    Private _view As ProductForm
    Private _model As Product

    Sub New()

    End Sub

    Public Sub New(ByVal view As ProductForm, ByVal model As Product)
        _view = view
        _model = model
    End Sub

    Public Sub Reset()
        _view.txtProductName.Text = ""
        _view.txtPrice.Text = ""

    End Sub

    Public Sub EnableBtn(ByVal bool As Boolean)
        _view.btnDelete.Enabled = bool
        _view.btnUpdate.Enabled = bool
        _view.btnNew.Enabled = bool
    End Sub

    Public Sub EnableSave(ByVal bool As Boolean)
        _view.btnSave.Enabled = bool
    End Sub

    Public Sub Save()
        If Not _model.IsProductExist() Then
            _model.Create()
            Me.LoadProduct()
            Me.Reset()
        Else
            MessageBox.Show("Product Already Exist! Try new product", "Alert", MessageBoxButtons.OK)
            Return
        End If

    End Sub

    Public Sub Edit()
        Try
            Dim dr As DataGridViewRow = _view.DataGridView1.SelectedRows(0)
            _view.txtProductName.Text = dr.Cells("ProductName").Value.ToString
            _view.txtPrice.Text = dr.Cells("Price").Value.ToString

            _view.cboCategory.Text = dr.Cells("Category").Value.ToString
            _view.cboCategory.SelectedValue = dr.Cells("CID").Value.ToString

            Dim status As String = Convert.ToInt32(dr.Cells("Status").Value)

            If status = 1 Then
                _view.cboStatus.SelectedIndex = 0
            Else
                _view.cboStatus.SelectedIndex = 1
            End If

            EnableBtn(True)
            EnableSave(False)

        Catch ex As Exception
            Console.WriteLine("Error: Edit product" & ex.ToString)
        End Try
        
    End Sub

    Public Sub LoadProduct()
        Dim ds As DataSet = New DataSet()
        ds = _model.GetProductDataSet()
        _view.DataGridView1.DataSource = ds.Tables(0)
    End Sub

    Public Sub LoadCategoryInCombo()
        Try
            Dim ds As DataSet = New DataSet()
            ds = New CategoryPresenter().GetCategoryDataSet()
            _view.cboCategory.DataSource = ds.Tables(0).DefaultView
            _view.cboCategory.DisplayMember = "CategoryName"
            _view.cboCategory.ValueMember = "Id"
        Catch ex As Exception
            Console.WriteLine("Error: Loading category at combobox" & ex.ToString)
        End Try
        
    End Sub

    Public Sub Update()
        If _view.txtPrice Is Nothing Or _view.txtPrice.Text = "" Then
            MessageBox.Show("Enter Price", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            _view.txtPrice.Focus()
            Return
        End If
        If _view.txtProductName Is Nothing Or _view.txtProductName.Text = "" Then
            MessageBox.Show("Enter Product Name", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            _view.txtProductName.Focus()
            Return
        End If
        Try
            If _view.DataGridView1.SelectedRows.Count > 0 Then
                Dim dr As DataGridViewRow = _view.DataGridView1.SelectedRows(0)
                _model.ProductId = dr.Cells("ID").Value.ToString
                If _model.Update() Then
                    Me.LoadProduct()
                    MessageBox.Show("Successfully Recorded", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("Process Failed", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show("Select Row First", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        

    End Sub

    Sub Delete()
        Dim dr As DataGridViewRow = _view.DataGridView1.SelectedRows(0)
        _model.ProductId = dr.Cells("ID").Value.ToString
        Dim response As String = MessageBox.Show("Are you sure You want to parmanently delete it?", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)
        If response = MsgBoxResult.Yes Then
            If _model.Delete() Then
                Me.LoadProduct()
                MessageBox.Show("Successfully Deleted", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Process Failed", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
        
    End Sub
End Class
