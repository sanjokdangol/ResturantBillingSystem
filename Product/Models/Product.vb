﻿'model could include methods that the presenter can Call

Imports System.Data.SqlClient
Imports ResturantBillingSystem

Public Class Product

    Dim _table As String = "Product"
    'properties
    Property ProductId As Integer
    Public Property ProductName As String
    Public Property Price As String
  
    Public Property CategoryID As String
    Public Property Status As Integer

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString

  

    Public Sub Create()
        q = "insert into " & _table & " (ProductName, Price, CategoryID, Status) values(@name, @price,@categoryId, @status)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", Me.ProductName)
            cmd.Parameters.AddWithValue("@price", Me.Price)
            cmd.Parameters.AddWithValue("@categoryId", Me.CategoryID)
            cmd.Parameters.AddWithValue("@status", Me.Status)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                UpdateChanges()
                MessageBox.Show("Successfully Recorded")

            Else
                MessageBox.Show("Process Failed")
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

    Public Function Read() As DataTable

        q = "SELECT * FROM " & _table & " order by ProductId Desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    Public Function Update() As Boolean
        Dim result As Boolean = False
        q = "UPDATE " & _table & " SET ProductName=@name, Price=@price, CategoryID=@categoryId, Status=@status WHERE ProductId = @PID"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", ProductName)
            cmd.Parameters.AddWithValue("@price", Price)
            cmd.Parameters.AddWithValue("@categoryId", CategoryID)
            cmd.Parameters.AddWithValue("@status", Status)
            cmd.Parameters.AddWithValue("@PID", ProductId)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Public Function Delete() As Boolean
        Dim result As Boolean = False
        q = "DELETE FROM " & _table & " WHERE ProductId = @PID"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@PID", Me.ProductId)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Sub UpdateChanges()
        Dim cmdBuilder As SqlCommandBuilder
        Try
            cmdBuilder = New SqlCommandBuilder(adapter)
            changes = ds.GetChanges()
            If changes IsNot Nothing Then
                adapter.Update(changes)
            End If
            MsgBox("Changes Done")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Function GetProductById(ByVal id As Integer) As DataTable
        q = "SELECT * FROM " & _table & " WHERE ProductId = @PID)"

        Try
            
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@PID", id)

            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)

            ds = New DataSet()
            adapter.Fill(ds, "Product")
            dt = ds.Tables("Product")
            con.Open()



            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                MessageBox.Show("Successfully Recorded")
            Else
                MessageBox.Show("Process Failed")
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return dt
    End Function

    Public Function GetProductByCategory(ByVal categoryId As String) As DataTable

        q = "SELECT * FROM " & _table & " WHERE CategoryID=@catId and status = 1 order by ProductName"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("catId", categoryId)

            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt
    End Function

    'return String
    'product price
    Public Function GetProductPrice(ByVal id As Integer) As String

        q = "SELECT Price FROM " & _table & " WHERE ProductId = @PID"

        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@PID", id)

            Dim r As SqlDataReader = cmd.ExecuteReader()
            While r.Read()
                Price = r(0)
            End While

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return Price
    End Function

    Public Function GetProductDataSet() As DataSet
        q = "select ProductId as ID,ProductName,Price, c.CategoryName as Category, c.Id as CID, p.Status  from " & _table & " as p join Category as c on p.CategoryID = c.Id order by p.ProductId desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return ds
    End Function

    Public Function IsProductExist() As Boolean
        Dim res As Boolean = False
        q = "select count(ProductName) from " & _table & " Where ProductName=@ProductName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("ProductName", ProductName)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsCustomerExist" & ex.ToString)
        End Try
        Return res
    End Function

    Sub SearchProduct(ByRef dataCollection As AutoCompleteStringCollection)


        Dim adapter As SqlDataAdapter = New SqlDataAdapter()
        Dim ds As DataSet = New DataSet()

        Dim sql As String = "SELECT DISTINCT ProductName FROM " & _table
        con = New SqlConnection(conString)
        Try
            con.Open()
            cmd = New SqlCommand(sql, con)
            adapter.SelectCommand = cmd
            adapter.Fill(ds)
            adapter.Dispose()
            cmd.Dispose()
            con.Close()
            Dim row As DataRow
            For Each row In ds.Tables(0).Rows
                dataCollection.Add(row(0).ToString())
            Next
        Catch ex As Exception
            MessageBox.Show("Can not open connection ! ")
        End Try
    End Sub

    Public Function GetProductID(ByVal name As String) As String

        q = "SELECT ProductId FROM " & _table & " WHERE ProductName = @PID"

        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@PID", name)

            Dim r As SqlDataReader = cmd.ExecuteReader()
            While r.Read()
                ProductId = r(0)
            End While

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return ProductId
    End Function
End Class
