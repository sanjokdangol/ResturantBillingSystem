﻿Imports ResturantBillingSystem
Public Class ProductForm
    Implements IProductView

    Public _product As Product = New Product()

    Public Property product As Product Implements IProductView.product
        Get
            Try
                _product.ProductName = txtProductName.Text.Trim()
                _product.Price = txtPrice.Text.Trim()

                _product.CategoryID = cboCategory.SelectedValue

                Dim status As String = cboStatus.Text

                If status = "Available" Then
                    _product.Status = 1
                Else
                    _product.Status = 0
                End If

            Catch ex As Exception
                Console.WriteLine(ex)
            End Try

            Return _product
        End Get

        Set(value As Product)
            _product = value
            txtProductName.Text = _product.ProductName
            txtPrice.Text = product.Price
            cboCategory.SelectedValue = _product.CategoryID
            cboStatus.Text = _product.Status.ToString
        End Set
    End Property

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim p As ProductPresenter = New ProductPresenter(Me, product)
        p.Save()
    End Sub


    Private Sub ProductForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As ProductPresenter = New ProductPresenter(Me, product)
        p.EnableBtn(False)

        cboStatus.SelectedIndex = 0

        p.LoadCategoryInCombo()
        p.LoadProduct()
    End Sub

    Private Sub DataGridView1_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.RowHeaderMouseClick
        Dim p As ProductPresenter = New ProductPresenter(Me, product)
        p.Edit()
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim p As ProductPresenter = New ProductPresenter(Me, product)
        p.Update()
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Dim p As ProductPresenter = New ProductPresenter(Me, product)
        p.Reset()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim p As ProductPresenter = New ProductPresenter(Me, product)
        p.Delete()
    End Sub

  

    Private Sub cboCategory_KeyUp(sender As Object, e As KeyEventArgs) Handles cboCategory.KeyUp
        If ((e.KeyCode = Keys.Back) Or _
 (e.KeyCode = Keys.Left) Or _
 (e.KeyCode = Keys.Right) Or _
 (e.KeyCode = Keys.Up) Or _
 (e.KeyCode = Keys.Delete) Or _
 (e.KeyCode = Keys.Down) Or _
 (e.KeyCode = Keys.PageUp) Or _
 (e.KeyCode = Keys.PageDown) Or _
 (e.KeyCode = Keys.Home) Or _
 (e.KeyCode = Keys.End)) Then

            Return
        End If
        Search.SearchOnComboBox(cboCategory)
    End Sub
End Class