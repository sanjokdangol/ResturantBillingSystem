﻿Imports ResturantBillingSystem
Imports System.Data.SqlClient

Public Class UserPresenter

    Dim _view As UsersForm
    Dim _model As User

    Dim _user As IUser

    Sub New(ByVal view As UsersForm, ByVal model As User)
        _view = view
        _model = model
        _user = view
    End Sub

    Public Sub Reset()
        _view.txtUserName.Text = ""
        _view.cboStatus.Text = ""
        _view.txtPassword.Text = ""
        _view.txtName.Text = ""
        _view.txtContactNo.Text = ""
        _view.txtEmail.Text = ""
        _view.txtUserName.Enabled = True
        _view.txtPassword.Enabled = True
        _view.txtConfirmPassword.Enabled = True
        _view.btnCreate.Enabled = True
        _view.btnDelete.Enabled = False
        _view.btnUpdate.Enabled = False
        _view.btnChangePassword.Enabled = False

    End Sub

    Public Sub ResetAtUpdate()
        _view.txtUserName.Enabled = False
        _view.txtPassword.Enabled = False
        _view.txtConfirmPassword.Enabled = False
        _view.btnCreate.Enabled = False
        _view.btnDelete.Enabled = True
        _view.btnUpdate.Enabled = True
        _view.btnChangePassword.Enabled = True
    End Sub

    

    Sub LoadData()
        Dim dt As DataTable = New DataTable()
        Dim user As User = New User()
        dt = user.Read()
        Try
            _view.DataGridView1.DataSource = dt.DefaultView.ToTable(True, "UserName", "Name", "Email", "ContactNo", "UserType", "Status", "UpdatedAt")

            _view.DataGridView1.ReadOnly = True
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        

        Reset()
    End Sub

    Sub Destroy()
        If MessageBox.Show("Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo) = DialogResult.Yes Then

            Dim u As User = New User()
            u.UserName = _view.DataGridView1.SelectedRows(0).Cells(0).Value.ToString()
            If u.Delete() Then
                _view.DataGridView1.Rows.RemoveAt(_view.DataGridView1.SelectedRows(0).Index)
            End If
            Reset()
        End If
    End Sub

    Sub Update()

        'u.UserName = _view.DataGridView1.SelectedRows(0).Cells(0).Value.ToString()
        

        If _model.Update() Then
            LoadData()
        End If
        Reset()
    End Sub

    Sub CreateUser()
        
        If (_model.Create()) Then
            LoadData()
        End If

    End Sub
End Class
