﻿Imports System.Data.SqlClient

Public Class User
    Public Shared IsAdmin = False
    Public Shared CurrentUser = ""
    Public Property UserName() As String
    Public UserType As String
    Public Password As String
    Public Name As String
    Public ContactNo As String
    Property Email() As String
    Public UpdatedAt As String
    Public Status As String

    Public Shared _table As String = "Registration"

    'DB variables
    Public Shared con As SqlConnection
    Public Shared cmd As SqlCommand
    Public Shared q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Public Shared conString As String = ConnectionString.GetConnectionString

    'CRUD

    Public Function Create() As Boolean

        Dim res As Boolean = False
        If UserName = "" Then
            MessageBox.Show("UserName is Required", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If
        If Name = "" Then
            MessageBox.Show("Name is Required", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If
        If Password = "" Then
            MessageBox.Show("Name is Required", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If
        If IsUserExist() Then
            MessageBox.Show("UserName Already Exists! Try With Another Name", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End If

        q = "insert into " & _table &
            "(UserName, Name, Password, UserType, ContactNo, Email, Status) " &
            "values(@userName, @name, @password, @userType, @contactNo, @email, @status)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@userName", UserName)
            cmd.Parameters.AddWithValue("@name", Name)
            cmd.Parameters.AddWithValue("@password", Password)
            cmd.Parameters.AddWithValue("@userType", UserType)
            cmd.Parameters.AddWithValue("@contactNo", ContactNo)
            cmd.Parameters.AddWithValue("@email", Email)
            cmd.Parameters.AddWithValue("@status", Status)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                res = True
            Else
                res = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return res
    End Function

    Public Function Read() As DataTable

        q = "SELECT * FROM " & _table & " order by JoiningDate"

        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, "Registration")
            dt = ds.Tables("Registration")
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return dt
    End Function

    Public Function Delete() As Boolean
        Dim res As Boolean = False

        Try
            q = "delete from " & _table & " where UserName=@id"
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@id", UserName)
            Dim row As Integer = cmd.ExecuteNonQuery()
            If row > 0 Then
                res = True
            Else
                res = False
            End If

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try


        Return res
    End Function

    Public Function Update() As Boolean
        Dim res As Boolean = False
        q = "UPDATE " & _table & " SET  Name=@name, UserType=@userType, ContactNo=@contactNo, Email=@email, Status=@status, UpdatedAt=@updatedAt WHERE UserName =@userName"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)

            cmd.Parameters.AddWithValue("@name", Name)
            cmd.Parameters.AddWithValue("@UserName", UserName)
            cmd.Parameters.AddWithValue("@userType", UserType)
            cmd.Parameters.AddWithValue("@contactNo", ContactNo)
            cmd.Parameters.AddWithValue("@email", Email)
            cmd.Parameters.AddWithValue("@status", Status)
            cmd.Parameters.AddWithValue("@updatedAt", Date.Now())

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                res = True
            Else
                res = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return res
    End Function

    Public Function IsOldPasswordMatched(ByVal username As String, ByVal oldPass As String) As Boolean
        Dim res As Boolean = False
        q = "SELECT count(UserName) FROM " & _table & " WHERE UserName=@userName AND Password=@oldPass"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@userName", username)
            cmd.Parameters.AddWithValue("@oldPass", oldPass)

            Dim row As Integer = cmd.ExecuteScalar()

            If row > 0 Then
                res = True
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return res
    End Function

    Public Function UpdatePassword(ByVal args As ArrayList) As Boolean
        Dim result As Boolean = False
        'MessageBox.Show(args.Item(0)) oldpass
        If args.Item(0) IsNot Nothing Then
            If IsOldPasswordMatched(UserName, args.Item(0)) Then
                'check confirm pass
                If args.Item(1) = args.Item(2) Then
                    q = "UPDATE " & _table & " SET Password=@password WHERE UserName =@userName"

                    Try
                        conString = ConnectionString.GetConnectionString
                        con = New SqlConnection(conString)
                        con.Open()

                        cmd = New SqlCommand(q, con)
                        cmd.Parameters.AddWithValue("@userName", UserName)
                        cmd.Parameters.AddWithValue("@password", args.Item(1))
                        cmd.Parameters.AddWithValue("@updatedAt", UpdatedAt)

                        Dim row As Integer = cmd.ExecuteNonQuery()

                        If row > 0 Then

                            Reset()
                            result = True
                        Else
                            result = False
                        End If

                        con.Close()

                    Catch ex As Exception
                        Console.WriteLine(ex)
                    End Try
                Else
                    MessageBox.Show("Confirm Password donot match with password!")
                End If

            Else
                MessageBox.Show("Incorrect Password!", "Alert!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
        Return result
    End Function

    Public Function IsUserExist() As Boolean
        Dim res As Boolean = False
        q = "select count(UserName) from " & _table & " Where UserName=@UserName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("UserName", UserName)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsBillNoExist" & ex.ToString)
        End Try
        Return res
    End Function

    Public Function IsUserAdmin() As Boolean
        Dim res As Boolean = False
        q = "select count(UserName) from " & _table & " Where UserName=@UserName and UserType = 'admin' and status = 1"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("UserName", UserName)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsBillNoExist" & ex.ToString)
        End Try
        Return res
    End Function
End Class
