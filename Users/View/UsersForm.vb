﻿Public Class UsersForm
    Implements IUser

    Dim _presenter As UserPresenter
    Private _model As User = New User()

    Private Sub UsersForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboStatus.SelectedIndex = 0
        cboUserType.SelectedIndex = 0
        cboSearchBy.SelectedIndex = 0
        _presenter = New UserPresenter(Me, user)
        _presenter.LoadData()
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        _presenter = New UserPresenter(Me, user)
        _presenter.CreateUser()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        _presenter = New UserPresenter(Me, user)
        _presenter.Destroy()
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        _presenter = New UserPresenter(Me, user)
        _presenter.Update()
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.SelectedRows.Count > 0 Then
                Dim selected As DataGridViewRow = DataGridView1.SelectedRows(0)
                txtUserName.Text = selected.Cells("UserName").Value.ToString
                cboStatus.Text = selected.Cells("Status").Value.ToString
                cboUserType.Text = selected.Cells("UserType").Value.ToString
                txtName.Text = selected.Cells("Name").Value.ToString
                txtContactNo.Text = selected.Cells("ContactNo").Value.ToString
                txtEmail.Text = selected.Cells("Email").Value.ToString
                txtConfirmPassword.Text = ""
                _presenter.ResetAtUpdate()
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        _presenter = New UserPresenter(Me, _model)
        _presenter.Reset()
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Try
            DataGridView1.DataSource.DefaultView.RowFilter = String.Format(" " + cboSearchBy.Text + " like '" + txtSearch.Text + "%'")

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Private Sub btnChangePassword_Click(sender As Object, e As EventArgs) Handles btnChangePassword.Click
        Dim frm As ChangePasswordForm = New ChangePasswordForm(Me)
        Me.Enabled = False
        frm.Show()
    End Sub

    Public Property user As User Implements IUser.user
        Get
            _model.UserName = txtUserName.Text
            _model.UserType = cboUserType.Text
            _model.Password = txtPassword.Text
            _model.Email = txtEmail.Text
            _model.Status = cboStatus.Text
            _model.ContactNo = txtContactNo.Text
            _model.Name = txtName.Text
            Return _model
        End Get
        Set(value As User)
            _model = value
            txtUserName.Text = _model.UserName
            cboUserType.Text = _model.UserType
            txtPassword.Text = _model.Password
            txtEmail.Text = _model.Email
            txtContactNo.Text = _model.ContactNo
            cboStatus.Text = _model.Status
        End Set
    End Property

    Sub ResetMe()
        txtUserName.Enabled = True
        txtPassword.Enabled = True
        txtConfirmPassword.Enabled = True
        btnCreate.Enabled = True
        btnDelete.Enabled = False
        btnUpdate.Enabled = False
        btnChangePassword.Enabled = False
    End Sub

End Class