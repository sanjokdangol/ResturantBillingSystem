﻿Public Class ChangePasswordForm

    Dim frm1 As UsersForm
    Sub New(ByVal _f1 As UsersForm)
        InitializeComponent()
        frm1 = _f1
    End Sub

    Private Sub btnUpdatePassword_Click(sender As Object, e As EventArgs) Handles btnUpdatePassword.Click
        Dim oldPass As String = txtOldPassword.Text.Trim
        Dim newPass As String = txtNewPassword.Text.Trim
        Dim confirmPass As String = txtConfirmNewPassword.Text.Trim
        Dim args As ArrayList = New ArrayList()

        args.Add(oldPass)
        args.Add(newPass)
        args.Add(confirmPass)

        Dim u As User = New User()
        If u.UpdatePassword(args) Then
            Me.Close()
            frm1.Enabled = True
            frm1.TopMost = True

        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
        frm1.Enabled = True
        frm1.TopMost = True
    End Sub
End Class