﻿Imports System.Data.SqlClient

Public Class LoginPresenter
    Dim _view As LoginForm
    Dim _model As User

    Sub New(ByVal view As LoginForm, ByVal model As User)
        _view = view
        _model = model
    End Sub


    Public Sub Login()
        Dim p As User = New User()
        Dim userExist As Boolean = p.IsOldPasswordMatched(_model.UserName, _model.Password)
        If userExist Then
            User.IsAdmin = _model.IsUserAdmin()
            User.CurrentUser = _model.UserName
            Dim frm As MainForm = New MainForm()
            frm.Show()
            _view.Hide()
        Else
            MessageBox.Show("InCorrect Username & Password", "Alert", MessageBoxButtons.OK)
        End If
    End Sub
End Class
