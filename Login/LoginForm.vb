﻿Public Class LoginForm
    Implements IUser

    Dim _model As User = New User()

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim p As LoginPresenter = New LoginPresenter(Me, user)
        p.Login()

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub cbShowPassword_CheckedChanged(sender As Object, e As EventArgs) Handles cbShowPassword.CheckedChanged

        If cbShowPassword.Checked Then
            txtPassword.PasswordChar = Nothing
        Else
            txtPassword.PasswordChar = "*"
        End If

    End Sub


    Public Property user As User Implements IUser.user
        Get
            _model.UserName = txtUsername.Text
            _model.Password = txtPassword.Text
            Return _model
        End Get
        Set(value As User)
            _model = value
            txtUsername.Text = _model.UserName
            txtPassword.Text = _model.Password

        End Set
    End Property
End Class
