﻿Public Class PrintFun

    Public Shared Function GetPrintingDataFromList(ByVal lv As ListView) As String(,)
        Dim items(,) As String
        Dim row As Integer = 0
        row = lv.Items.Count
        Dim i, j As Integer
        items = New String(row - 1, 4) {}
        If (row > 0) Then
            i = 0
            While i < row
                j = 0
                While j < 5
                    items(i, j) = lv.Items(i).SubItems(j).Text
                    j = j + 1
                End While
                i = i + 1
            End While
        End If
        Return items
    End Function

    Public Shared Function GetPrintingDataFromGrid(ByVal gv As DataGridView) As String(,)
        Dim items(,) As String
        Dim row As Integer = 0
        'count row
        row = gv.Rows.Count
        Dim i, j As Integer
        items = New String(row - 1, 5) {}
        If (row > 0) Then
            i = 0
            While i < row - 1
                j = 0
                While j < gv.ColumnCount
                    If j = 0 Then
                        items(i, j) = i + 1
                    End If
                    items(i, j + 1) = gv.Rows(i).Cells(j).Value
                    j = j + 1
                End While
                i = i + 1
            End While
        End If
        Return items
    End Function

    '// Figure out how wide each column should be.
    Public Shared Function FindColumnWidths(ByVal gr As Graphics, ByVal data(,) As String, ByVal header_font As Font, ByVal Headers() As String, ByVal body_font As Font, ByVal count As Integer) As Integer()
        '// Make room for the widths.
        Dim widths(Headers.Length) As Integer
        Try
            '// Find the width for each column.
            Dim col As Integer = 0
            While col < widths.Length - 1

                '// Check the column header.
                widths(col) = gr.MeasureString(Headers(col), header_font).Width

                '//Console.Write(values.GetUpperBound(0));
                Dim row As Integer = 0
                While row < count

                    Dim value_width As Integer = gr.MeasureString(data(row, col), body_font).Width
                    If widths(col) < value_width Then
                        widths(col) = value_width
                    End If
                    row = row + 1
                End While

                '// Add some extra space.
                'widths(col) += 5
                col = col + 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return widths

    End Function
End Class
