﻿Public Class PrintFooter
    Inherits Print

    Property _e As Printing.PrintPageEventArgs

    Dim startX As Integer
    Property startY As Integer
    Dim Offset As Integer
    Property stringFormat As StringFormat

    Public Sub Print(ByVal sender As Object, ByVal e As Printing.PrintPageEventArgs)
        _e = e
        e.Graphics.PageUnit = GraphicsUnit.Pixel
        FooterFont = New Font("Courier New", 8, FontStyle.Bold)
        line_spacing = 25
        startX = 10.0F
        Offset = 0

        stringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

        Try
            PrintSeperatorLine()

            If Not SubTotal = "" Then
                PrintStringWithRec(SubTotal)
            End If

            If Not DiscountAmount = "" Then
                PrintStringWithRec(DiscountAmount)
            End If

            If Not VATAmount = "" Then
                PrintStringWithRec(VATAmount)
            End If

            If Not GrandTotal = "" Then
                PrintStringWithRec(GrandTotal)
            End If
            If Not Cash = "" Then
                PrintStringWithRec(Cash)
            End If
            If Not Change = "" Then
                PrintStringWithRec(Change)
            End If

            If Not CustomerName = "" Then
                PrintSeperatorLine()
                PrintStringWithRec(CustomerName)
            End If
            If Not ContactNo = "" Then
                PrintStringWithRec(ContactNo)
            End If
            PrintSeperatorLine()
            If Not WaiterName = "" Then
                PrintStringWithRec(WaiterName)
            End If
            If Not FooterMessage = "" Then
                _e.Graphics.DrawString(FooterMessage, FooterFont,
                    New SolidBrush(Color.Black), New Rectangle(startX, startY + Offset, 375, 100), stringFormat)
                Offset = Offset + line_spacing
            End If

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Sub PrintStringWithRec(ByVal text As String)
        Dim blackPen As New Pen(Color.Black, 1)
        ' Draw rectangle to screen.
        '_e.Graphics.DrawRectangle(blackPen, startX, startY + Offset, 430, 40)
        _e.Graphics.DrawString(text, FooterFont,
                    New SolidBrush(Color.Black), New Rectangle(startX, startY + Offset, 375, 40), stringFormat)
        Offset = Offset + line_spacing
    End Sub

    Sub PrintSeperatorLine()
        Dim u1 As String = "----------------------------------"
        _e.Graphics.DrawString(u1, FooterFont,
         New SolidBrush(Color.Black), startX, startY + Offset)
        Offset = Offset + line_spacing
    End Sub
End Class


