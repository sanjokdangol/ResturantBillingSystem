﻿
Public Class PrintHeader
    Inherits Print

    Property _e As Printing.PrintPageEventArgs

    Dim startX As Integer
    Dim startY As Integer
    Dim Offset As Integer
    Property stringFormat As StringFormat

    'print header and return height of header to start body
    Public Function Print(ByVal sender As Object, ByVal e As Printing.PrintPageEventArgs) As Integer
        _e = e
        e.Graphics.PageUnit = GraphicsUnit.Pixel
        line_spacing = 20
        startX = 0
        startY = 0 '10
        Offset = 0

        stringFormat = New StringFormat()
        stringFormat.Alignment = StringAlignment.Center
        stringFormat.LineAlignment = StringAlignment.Center


        ''Dim CentrePage As String = Convert.ToString(((e.PageBounds.Width * 0.264583333) / 2) - (e.Graphics.MeasureString("ok", f).Width / 2))
        'Console.WriteLine(e.PageBounds.Width * 0.264583333)
        'Console.WriteLine(e.PageBounds.Width)
        ''e.Graphics.DrawString("ok", f, New SolidBrush(Color.Black), Int(CentrePage), 17)

        HotelName = "RATOBHALE FASTFOOD & TONDOORI RESTURANT"
        Address = "Samakhushi, Kathmandu"
        Header = {"Sn", "Particulars", "Rate", "Qty", "Amount"}
        HeaderFont = New Font("Courier New", 8, FontStyle.Bold)
        Try
            If Not TableNo = "" Then
                _e.Graphics.DrawString(TableNo, New Font("Courier New", 12, FontStyle.Bold),
                    New SolidBrush(Color.Black), New Rectangle(startX, startY + Offset, 415, 80), stringFormat)
                Offset = Offset + line_spacing + 30
            End If

            If Not HotelName = "" And HotelName IsNot Nothing Then
                PrintStringWithRec(HotelName)
                Offset = Offset + 5
            End If

            If Not Address = "" And Address IsNot Nothing Then
                PrintStringWithRec(Address)
            End If
            Offset = Offset + line_spacing
            If Not String.IsNullOrEmpty(PanNo) Then
                PrintString(PanNo)
            End If

            If Not String.IsNullOrEmpty(Contact) Then
                PrintString(Contact)
            End If
            If Not String.IsNullOrEmpty(BillNo) Then
                PrintString(BillNo)
            End If
            If Not String.IsNullOrEmpty(BillDate) Then
                PrintString(BillDate)
            End If

            If Not String.IsNullOrEmpty(InvoiceType) Then
                PrintStringWithRec(InvoiceType)
            End If

            Offset = Offset + line_spacing

            PrintString("-----------------------------------")

            If Header.Length > 0 Then
                PrintSubject()
            End If

            PrintString("-----------------------------------")

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return Offset
    End Function

    Sub PrintString(ByVal text As String)
        _e.Graphics.DrawString(text, HeaderFont, New SolidBrush(Color.Black), startX, startY + Offset)
        Offset = Offset + line_spacing
    End Sub

    Sub PrintStringWithRec(ByVal text As String)
        Dim blackPen As New Pen(Color.Black, 1)
        ' Draw rectangle to screen.
        ' _e.Graphics.DrawRectangle(blackPen, startX, startY + Offset, 415, 18)
        _e.Graphics.DrawString(text, HeaderFont,
                    New SolidBrush(Color.Black), New Rectangle(startX, startY + Offset, 400, 50), stringFormat)
        Offset = Offset + line_spacing
    End Sub

    Sub PrintSubject()
        Dim leftAlign As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)
        Dim align As StringFormat = New StringFormat()
        Dim gap As Integer = 5
        Dim inc As Integer = 13

        Dim l As Integer = 10
        Dim j As Integer = 0
        Dim x As Integer = -15 '5
        While j < Header.Length
            Select Case j
                Case 0
                    x += 0 + inc
                    l = 30 + inc
                    align = leftAlign
                Case 1
                    x += 30 + gap + inc
                    l = 110 + inc + 30
                    align = New StringFormat()
                Case 2
                    'rate
                    x += 110 + inc + 30
                    l = 40 + inc
                    align = leftAlign
                Case 3
                    'qty
                    x += 40 + gap + inc
                    l = 30 + inc
                    align = leftAlign
                Case 4
                    x += 30 + inc
                    l = 60 + inc + 10
                    align = leftAlign
            End Select
            '_e.Graphics.DrawString(Header(j), HeaderFont, Brushes.Blue, x, startY + Offset)
            Dim blackPen As New Pen(Color.Black, 1)
            ' Draw rectangle to screen.
            ' _e.Graphics.DrawRectangle(blackPen, x, startY + Offset, l, 18)

            _e.Graphics.DrawString(Header(j), HeaderFont, New SolidBrush(Color.Black), New Rectangle(x, startY + Offset, l, 20), align)
            j += 1
        End While
        startX = 0
        Offset = Offset + line_spacing
    End Sub

End Class
