﻿Public Interface IFastPrinting
    Property _currentPrintingRow As Integer
    Property x As Integer
    Property y As Integer
    Property header_height As Integer
    Property count As Integer
    Property row As Integer
    Property body As PrintBody
End Interface
