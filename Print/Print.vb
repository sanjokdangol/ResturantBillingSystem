﻿Public Class Print
    Inherits Customer

    Public Property HotelName As String
    Public Property Address As String
    Public Property Contact As String
    Property InvoiceType As String
    Property PanNo As String

    Public Property BillNo As String
    Public Property BillDate As String
    Public Property WaiterName As String
    Public Property TableNo As String

    Public Property PrintData As String(,)
    Public Property Count As Integer

    Public Property SubTotal As String
    Public Property DiscountPer As String
    Public Property DiscountAmount As String
    Public Property VATPer As String
    Public Property VATAmount As String
    Public Property GrandTotal As String
    Public Property Change As String
    Public Property Cash As String

    Public Property Header As String()
    Public Property ColNum As Integer

    Property HeaderFont As Font
    Property BodyFont As Font
    Property FooterFont As Font
    Property FooterMessage As String

    Property line_spacing As Integer

End Class
