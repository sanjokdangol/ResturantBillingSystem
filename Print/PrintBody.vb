﻿Public Class PrintBody
    Inherits Print

    Property currentPrintingRow As Integer

    Dim i As Integer
    Property x As Integer
    Property y As Integer
    Property header_height As Integer 'top margin to start body after header
    Property row As Integer

    Sub Print(ByVal e As Printing.PrintPageEventArgs)
        e.Graphics.PageUnit = GraphicsUnit.Pixel

        BodyFont = New Font("Courier New", 8)

        line_spacing = 25
        Dim leftAlign As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)
        Dim align As StringFormat = New StringFormat()

        Count = Decimal.Ceiling(row / 60)

        Dim k As Integer = 0
        Try
            If _currentPrintingRow > 0 And Not _currentPrintingRow = Count - 1 Then
                'second and middle pages
                k = i + 65
            ElseIf _currentPrintingRow = Count - 1 Then
                'adjust body top margin if first page = lastpage or single page/60
                If (row < 50) Then
                    y = Me.header_height + 5
                End If
                k = row
                'get remaining row of lastpage

            Else
                'number of rows for first page printing/60
                If (row < 50) Then
                    k = i + row
                Else
                    k = i + 50
                End If

                'height of header
                y = Me.header_height + 5
            End If
            Dim gap As Integer = 5

            Dim l As Integer = 10
            Dim inc As Integer = 13 '20
            While i < k
                x = 0 '5
                'j must be inside k while loop
                'donot move it from here
                Dim j As Integer = 0
                While j < ColNum
                    'adjust x co-ordinates of column
                    Select Case j
                        Case 0
                            x += 0
                            l = 30 + inc
                            align = leftAlign
                        Case 1
                            'Particulars
                            x += 30 + gap + inc
                            l = 110 + inc + 30
                            align = New StringFormat()
                        Case 2
                            'rate
                            x += 110 + inc + 30
                            l = 40 + inc
                            align = leftAlign
                        Case 3
                            'qty
                            x += 40 + gap + inc
                            l = 30 + inc
                            align = leftAlign
                        Case 4
                            x += 30 + inc
                            l = 50 + inc + 10
                            align = leftAlign
                    End Select


                    Dim str As String

                    str = PrintData(i, j)

                    If j = 1 And Not String.IsNullOrEmpty(str) Then
                        If str.Length > 14 Then
                            str = str.Substring(0, 14)
                        End If

                    End If
                    Dim blackPen As New Pen(Color.Black, 1)
                    ' Draw rectangle to screen.
                    'e.Graphics.DrawRectangle(blackPen, x, y, l, 40)
                    e.Graphics.DrawString(str.ToUpper(), BodyFont, New SolidBrush(Color.Black), New Rectangle(x, y, l, 20), align)
                    j += 1
                End While
                y += line_spacing
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    'used for testing multi page printing. demo datas
    Function GetDemoData() As String(,)

        Dim demoData(row, 5) As String
        Dim i As Integer = 0
        While i < row
            Dim j As Integer = 0
            While j < 5
                If j = 0 Then
                    demoData(i, j) = "123"
                End If
                If j = 1 Then
                    demoData(i, j) = "HotLeamonJuice"
                End If
                If j = 2 Then
                    demoData(i, j) = "2000"
                End If
                If j = 3 Then
                    demoData(i, j) = "500"
                End If
                If j = 4 Then
                    demoData(i, j) = "50000"
                End If
                j += 1
            End While
            i += 1
        End While
        Return demoData
    End Function

End Class
