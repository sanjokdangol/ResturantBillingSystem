﻿Imports ResturantBillingSystem
Imports System.Data.SqlClient

Public Class CategoryPresenter

    Dim _view As CategoryForm
    Dim _model As Category

    Dim _table As String = "Category"

    'Database variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim sBuilder As SqlCommandBuilder
    Dim adapter As SqlDataAdapter
    Dim conString As String = ConnectionString.GetConnectionString
    Dim q As String
    Dim dt As DataTable
    Dim ds As DataSet
    Dim changes As DataSet

    Sub New()
    End Sub

    Sub New(ByVal view As Form, ByVal model As Category)
        _view = view
        _model = model
        BindModelData()
    End Sub

    Public Sub BindModelData()
        _model.CategoryName = _view.txtCategoryName.Text.Trim()
        _model.Status = _view.cboStatus.Text
    End Sub

    Public Sub Reset()
        _view.txtCategoryName.Text = ""
        _view.txtSearch.Text = ""
        _view.cboStatus.Text = ""
        _view.btnCreate.Enabled = False
        _view.btnDelete.Enabled = False
        _view.btnUpdate.Enabled = False
    End Sub

    Public Sub ResetAtUpdate()
        _view.btnCreate.Enabled = False
        _view.btnDelete.Enabled = True
        _view.btnUpdate.Enabled = True

    End Sub

    'CRUD
    Public Sub Create()
        If _model.IsCategoryExist() Then
            MessageBox.Show("Category Name already exists! Try Different Name", "Alert", MessageBoxButtons.OK)
            Return
        End If

        q = "insert into " & _table & "(CategoryName, Status) values(@name, @status)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", _model.CategoryName)
            cmd.Parameters.AddWithValue("@status", _model.Status)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                Read()
                _view.lblSuccessMsg.Text = "Successfully Recorded"
            Else
                MessageBox.Show("Process Failed")
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

    Public Sub Read()

        q = "SELECT * FROM " & _table & " where Status = 'Avialable' order by Id Desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, "Category")
            dt = ds.Tables("Category")
            con.Close()
            _view.DataGridView1.DataSource = dt
            _view.DataGridView1.ReadOnly = True

            Reset()
            _view.DataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

    Public Function GetCategory() As DataTable
        q = "SELECT * FROM " & _table & " Where Status = 'Avialable' order by Id Desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, "Category")
            dt = ds.Tables("Category")
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return dt
    End Function

    Public Function GetCategoryDataSet() As DataSet
        q = "SELECT * FROM " & _table & " where Status = 'Avialable' order by Id Desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, "Category")

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return ds
    End Function

    Public Sub Delete()
        If MessageBox.Show("Do you want to delete this row ?", "Delete", MessageBoxButtons.YesNo) = DialogResult.Yes Then
            Dim id As Integer
            id = Integer.Parse(_view.DataGridView1.SelectedRows(0).Cells(0).Value)

            Try
                q = "delete from " & _table & " where Id=@id"
                con = New SqlConnection(conString)
                con.Open()

                cmd = New SqlCommand(q, con)
                cmd.Parameters.AddWithValue("@id", id)
                Dim row As Integer = cmd.ExecuteNonQuery()
                If row > 0 Then
                    _view.DataGridView1.Rows.RemoveAt(_view.DataGridView1.SelectedRows(0).Index)
                    Reset()
                    _view.lblSuccessMsg.Text = "Successfully Deleted"
                Else
                    _view.lblErrorMsg.Text = "Process Failed"

                End If

            Catch ex As Exception
                Console.WriteLine(ex)
            End Try



        End If
    End Sub

    Public Sub Update()
        Dim id As Integer

        Dim selected As DataGridViewRow = _view.DataGridView1.SelectedRows(0)
        id = Integer.Parse(selected.Cells(0).Value)

        q = "UPDATE " & _table & " SET CategoryName=@name, Status=@status WHERE Id =@id"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", _view.txtCategoryName.Text)
            cmd.Parameters.AddWithValue("@status", _view.cboStatus.Text)
            cmd.Parameters.AddWithValue("@id", id)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                Read()
                Reset()
                _view.lblSuccessMsg.Text = "Successfully Updated"
            Else
                _view.lblErrorMsg.Text = "Process Failed"
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

End Class
