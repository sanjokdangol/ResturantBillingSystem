﻿Imports System.Data.SqlClient
Public Class Category

    Property CategoryId As Integer
    Property CategoryName As String
    Property Status As String

    Dim _table As String = "Category"

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString

    Public Function IsCategoryExist() As Boolean

        Dim res As Boolean = False
        q = "select count(CategoryName) from " & _table & " Where CategoryName=@CategoryName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("CategoryName", CategoryName)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsCategoryExist: " & ex.ToString)
        End Try
        Return res
    End Function
End Class
