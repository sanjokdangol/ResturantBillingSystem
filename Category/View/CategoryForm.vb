﻿Imports ResturantBillingSystem
Public Class CategoryForm
    Dim _presenter As CategoryPresenter
    Private _model As Category = New Category()

    Private Sub CategoryForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboStatus.SelectedIndex = 0
        cboSearchBy.SelectedIndex = 0
        _presenter = New CategoryPresenter(Me, _model)
        _presenter.Read()
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        _presenter = New CategoryPresenter(Me, _model)
        _presenter.Create()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        _presenter = New CategoryPresenter(Me, _model)
        _presenter.Delete()
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        _presenter = New CategoryPresenter(Me, _model)
        _presenter.Update()
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            If DataGridView1.SelectedRows.Count > 0 Then
                Dim selected As DataGridViewRow = DataGridView1.SelectedRows(0)
                txtCategoryName.Text = selected.Cells("CategoryName").Value.ToString
                cboStatus.Text = selected.Cells("Status").Value.ToString
                _presenter.ResetAtUpdate()
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        _presenter = New CategoryPresenter(Me, _model)
        _presenter.Reset()
    End Sub

    Private Sub txtCategoryName_TextChanged(sender As Object, e As EventArgs) Handles txtCategoryName.TextChanged
        btnCreate.Enabled = True
        If txtCategoryName.Text = Nothing Then
            btnCreate.Enabled = False
        End If

    End Sub


    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Try
            DataGridView1.DataSource.DefaultView.RowFilter = String.Format(" " + cboSearchBy.Text + " like '" + txtSearch.Text + "%'")

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        _presenter = New CategoryPresenter(Me, _model)
        _presenter.Read()
    End Sub
End Class