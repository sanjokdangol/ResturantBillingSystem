﻿Imports System.Data.SqlClient

Public Class Transation
    Property Id As Integer
    Property BillNo As String
    Property BillDate As String
    Property SubTotal As String
    Property VATPer As String
    Property VATAmount As String
    Property DiscountPer As String
    Property DiscountAmount As String
    Property GrandTotal As String
    Property Cash As String
    Property Change As String
    Property Remarks As String
    Property ServiceCharge As String
    Property CustomerID As String
    Public Shared ServiceChargePer As Double = 0.1

    Dim _table As String = "Invoice_Info"

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString

    Public Function Create() As Boolean
        Dim result = False
        If ServiceCharge = Nothing Then
            ServiceCharge = 0
        End If
        If Remarks = Nothing Then
            Remarks = "Cash"
        End If
        If CustomerID = Nothing Then
            CustomerID = 0
        End If

        q = "insert into " & _table & " (BillNo, BillDate, SubTotal, VATPer, VATAmount, DiscountPer, DiscountAmount, GrandTotal, Cash, Change, ServiceCharge, Remarks, CustomerID, HotelID, CurrencyID) values(@BillNo, @BillDate, @SubTotal, @VATPer, @VATAmount, @DiscountPer, @DiscountAmount, @GrandTotal, @Cash, @Change, @ServiceCharge, @Remarks, @CustomerID, @HotelID, @CurrencyID)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@BillNo", Me.BillNo)
            cmd.Parameters.AddWithValue("@BillDate", Date.Now())
            cmd.Parameters.AddWithValue("@SubTotal", Me.SubTotal)
            cmd.Parameters.AddWithValue("@VATPer", Me.VATPer)
            cmd.Parameters.AddWithValue("@VATAmount", Me.VATAmount)
            cmd.Parameters.AddWithValue("@DiscountPer", Me.DiscountPer)
            cmd.Parameters.AddWithValue("@DiscountAmount", DiscountAmount)
            cmd.Parameters.AddWithValue("@GrandTotal", GrandTotal)
            cmd.Parameters.AddWithValue("@Cash", Cash)
            cmd.Parameters.AddWithValue("@Change", Change)
            cmd.Parameters.AddWithValue("@ServiceCharge", ServiceCharge)
            cmd.Parameters.AddWithValue("@Remarks", Remarks)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            cmd.Parameters.AddWithValue("@HotelID", 1)
            cmd.Parameters.AddWithValue("@CurrencyID", 1)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create InvoiceInfo" & ex.ToString)
        End Try

        Return result

    End Function

    Public Function Read() As DataTable

        q = "SELECT BillNo as 'BillNo', BillDate as 'Date', SubTotal as 'Total', VATPer as 'VAT(%)', VATAmount, DiscountPer as 'Discount(%)',DiscountAmount, GrandTotal, Cash as 'Cash_Received', Change as 'Change_Amount',Remarks as SalesType FROM " & _table &
            " where cast(BillDate as Date) = cast(getdate() as Date) order by BillDate Desc"

        Dim df As DBFun = New DBFun()

        Return df.GetDataTableByQuery(q, _table)

    End Function

    Public Function GetInvoiceListByDate(ByVal fromDate As String, ByVal toDate As String) As DataTable
        Dim fDate, tDate As Date
        Date.TryParse(fromDate, fDate)
        Date.TryParse(toDate, tDate)
        q = "select BillNo as 'BillNo', BillDate as 'Date', SubTotal as 'Total', VATPer as 'VAT(%)', VATAmount, DiscountPer as 'Discount(%)',DiscountAmount, GrandTotal, Cash as 'Cash_Received', Change as 'Change_Amount',Remarks as SalesType from Invoice_Info as i " +
           "where cast(BillDate as Date) >= cast(@fromDate as Date) and cast(BillDate as Date) <= cast(@toDate  as Date) " +
           "order by BillDate DESC"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("fromDate", fDate)
            cmd.Parameters.AddWithValue("toDate", tDate)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at GetInvoiceListByDate " & ex.ToString)
        End Try
        Return dt
    End Function

    Public Function ReadCreditBills() As DataTable

        q = "SELECT CustomerName, sum(GrandTotal) as Total, sum(Cash) as Paid, sum(Change) as 'Due_Amount', b.CustomerID, ContactNo FROM Invoice_Info as b" &
           " inner join Customer as c on c.CustomerId = b.CustomerID where Remarks = 'due' group By b.CustomerID, CustomerName, ContactNo order by CustomerName"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    Public Function GetTransactionDetailsByBillNo() As DataTable

        q = "SELECT BillNo, GrandTotal, Cash, Change, DiscountPer, DiscountAmount, VATAmount,SubTotal, BillDate FROM " & _table & " where BillNo = @BillNo"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@BillNo", BillNo)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function


End Class
