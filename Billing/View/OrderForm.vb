﻿Public Class OrderForm
    Implements IBillOrder
    Implements IProductView

    Property BillID As String
    Property TblNo As String
    Property WaiterName As String
    Property WaiterID As String

    Dim tempTbl As String

    Property pr As Product = New Product()
    Public _m As BillOrder = New BillOrder()

    Public _table As TableForm

    Sub New(ByVal view As Form)

        ' This call is required by the designer.
        InitializeComponent()
        _table = view

        ' Add any initialization after the InitializeComponent() call.
  
    End Sub
    Dim p As OrderPresenter = New OrderPresenter(Me, product, bill)

    Private Sub OrderForm_Load_1(sender As Object, e As EventArgs) Handles MyBase.Load
        
        'tempTbl = _table.txtTblNo.Text
        'bill.HotelTableID = _table.txtTblNo.Text
        tempTbl = TblNo
        bill.HotelTableID = TblNo
        txtTableNumber.Text = TblNo

        Dim o As OrderPresenter = New OrderPresenter(Me, product, New BillOrder())
        o.LoadForm()
        cboCategory.Focus()
    End Sub


    Private Sub cboCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCategory.SelectedIndexChanged
        Dim o As OrderPresenter = New OrderPresenter(Me, product, bill)
        o.LoadProductInComboByCategory()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim p As OrderPresenter = New OrderPresenter(Me, product, bill)
        p.AddProductToBill()

    End Sub

    Private Sub cboProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProduct.SelectedIndexChanged
        Dim o As OrderPresenter = New OrderPresenter(Me, product, bill)
        o.SearchProduct()
       
    End Sub

    Private Sub NumericUpDown1_ValueChanged(sender As Object, e As EventArgs) Handles NumericUpDown1.ValueChanged
        Dim p As OrderPresenter = New OrderPresenter(Me, product, bill)
        p.GetSubTotal()
    End Sub

    Private Sub txtPrice_TextChanged(sender As Object, e As EventArgs) Handles txtPrice.TextChanged
        Dim p As OrderPresenter = New OrderPresenter(Me, product, bill)
        p.GetSubTotal()
    End Sub

    Private Sub NumericUpDown1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NumericUpDown1.KeyPress
        Dim p As OrderPresenter = New OrderPresenter(Me, product, bill)
        p.GetSubTotal()
    End Sub


    Private Sub btnOrder_Click(sender As Object, e As EventArgs) Handles btnOrder.Click
        Dim p As OrderPresenter = New OrderPresenter(Me, product, bill)
        p.OrderBill()
    End Sub


    Private Sub cboWaiter_Click(sender As Object, e As EventArgs) Handles cboWaiter.Click
        p.LoadWaiterList()
    End Sub

    Private Sub cboCategory_Click(sender As Object, e As EventArgs) Handles cboCategory.Click
        p.LoadCategoryInCombo()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Dim p As OrderPresenter = New OrderPresenter(Me)
        p.RemoveItemFromList()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim p As OrderPresenter = New OrderPresenter(Me)
        p.IncreaseQty()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim p As OrderPresenter = New OrderPresenter(Me)
        p.DecreaseQty()
    End Sub

    Private Sub cboCategory_KeyUp(sender As Object, e As KeyEventArgs) Handles cboCategory.KeyUp
        ' Do nothing for some keys such as navigation keys.
        If ((e.KeyCode = Keys.Back) Or _
    (e.KeyCode = Keys.Left) Or _
    (e.KeyCode = Keys.Right) Or _
    (e.KeyCode = Keys.Up) Or _
    (e.KeyCode = Keys.Delete) Or _
    (e.KeyCode = Keys.Down) Or _
    (e.KeyCode = Keys.PageUp) Or _
    (e.KeyCode = Keys.PageDown) Or _
    (e.KeyCode = Keys.Home) Or _
    (e.KeyCode = Keys.End)) Then

            Return
        End If

        Search.SearchOnComboBox(cboCategory)
        If e.KeyCode = Keys.Enter Then
            cboProduct.Focus()
        End If
    End Sub


    Private Sub txtTableNumber_TextChanged(sender As Object, e As EventArgs) Handles txtTableNumber.TextChanged
        If Not txtTableNumber.Enabled = False And Not tempTbl = txtTableNumber.Text Then
            If TablePresenter.IsBusy(txtTableNumber.Text, _table.ListView1) Then
                MessageBox.Show("Table No. " & txtTableNumber.Text & "Is Reserved Now! Try Different Table")
                txtTableNumber.Text = _m.HotelTableID
                Return
            End If
        End If
    End Sub

    Private Sub cboProduct_KeyDown(sender As Object, e As KeyEventArgs) Handles cboProduct.KeyDown
        If e.KeyCode = Keys.Enter Then
            Button1.Focus()
        End If
    End Sub

    Private Sub cboCategory_KeyDown(sender As Object, e As KeyEventArgs) Handles cboCategory.KeyDown
        If e.KeyCode = Keys.Enter Then
            cboProduct.Focus()
        End If
    End Sub

    Private Sub cboWaiter_KeyDown(sender As Object, e As KeyEventArgs) Handles cboWaiter.KeyDown
        If e.KeyCode = Keys.Enter Then
            Dim p As OrderPresenter = New OrderPresenter(Me, bill)
            p.OrderBill()
            btnOrder.Focus()
        End If
    End Sub


    Public Property product As Product Implements IProductView.product
        Get
            Try
                pr.ProductName = cboProduct.Text
                pr.Price = txtPrice.Text
                'pr.ProductId = cboProduct.ValueMember

            Catch ex As Exception

            End Try
            Return pr
        End Get
        Set(value As Product)

        End Set
    End Property

    Public Property bill As BillOrder Implements IBillOrder.bill
        Get
            Try

                _m.BillID = txtBill.Text
                _m.HotelTableID = txtTableNumber.Text
                _m.WaiterID = cboWaiter.SelectedValue.ToString
                _m.Total = txtGrandTotal.Text
                _m.Quantity = NumericUpDown1.Text

            Catch ex As Exception
                Console.WriteLine("Error:getter&setter OrderForm " & ex.ToString)
            End Try

            Return _m
        End Get
        Set(value As BillOrder)
            _m = value
            txtBill.Text = _m.BillID
            txtTableNumber.Text = _m.HotelTableID
            cboWaiter.SelectedValue = _m.WaiterID
        End Set
    End Property
End Class