﻿Imports System.Data.SqlClient


Public Class PayBillForm

    Implements ITransaction
    Implements IFastPrinting

    Property salesType As String = ""
    Dim CustomerName As String = ""
    Public _table As TableForm
    Property _t As Transation = New Transation()
    Dim _ar() As String

    Sub New(ByVal array() As String, ByVal table As TableForm)

        ' This call is required by the designer.
        InitializeComponent()
        _ar = array
        ' Add any initialization after the InitializeComponent() call.
        _table = table
    End Sub

    Private Sub PayBillForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.InitValues()

        Dim billNo As String = ""
        Dim tableNo As String = ""
        Dim total As String = ""
        Dim waiter As String = ""
        Dim i As Integer = 0

        For i = 0 To _ar.Length - 1
            billNo = _ar(i)
            i += 1
            tableNo = _ar(i)
            i += 1
            total = _ar(i)
            i += 1
            waiter = _ar(i)
        Next
        txtWaiter.Text = waiter
        txtTableNo.Text = tableNo
        txtBillNo.Text = billNo
        txtSubTotal.Text = total
        'fill listview with items
        LoadBill(billNo)
    End Sub

    'load bill items in listview
    Public Sub LoadBill(ByVal billNo As String)

        ListView1.Items.Clear()
        Dim reader As SqlDataReader
        Try
            Dim order As BillOrder = New BillOrder()
            reader = order.GetOrderListByBillNo(billNo)
            Dim i As Integer = 0
            While (reader.Read())
                i = i + 1
                ListView1.Items.Add(New ListViewItem(
                            New String() {
                                        i,
                                        reader(0).ToString(),
                                        reader(1).ToString(),
                                        reader(2).ToString(),
                                        reader(3).ToString()
                            }))
            End While
        Catch ex As Exception
            Console.WriteLine("Error: LoadBill PayBillForm " & ex.ToString)
        End Try

    End Sub


    Private Sub txtSubTotal_TextChanged(sender As Object, e As EventArgs) Handles txtSubTotal.TextChanged
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetGrandTotal()
    End Sub

    Private Sub txtCash_TextChanged(sender As Object, e As EventArgs) Handles txtCash.TextChanged
        
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetChangeAmount()
    End Sub

    Private Sub chkTax_CheckedChanged(sender As Object, e As EventArgs) Handles chkTax.CheckedChanged
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetTaxAmount()
    End Sub

    Public Property transaction As Transation Implements ITransaction.transaction

        Get
            _t.BillNo = txtBillNo.Text
            _t.BillDate = DateTimePicker1.Text
            _t.SubTotal = txtSubTotal.Text
            _t.VATPer = 13
            _t.VATAmount = txtTaxAmt.Text
            _t.DiscountPer = txtDiscount.Text
            _t.DiscountAmount = txtDiscountAmt.Text
            _t.GrandTotal = txtGrandTotal.Text
            _t.Cash = txtCash.Text
            _t.Change = txtChange.Text
            _t.ServiceCharge = txtServiceCharge.Text
            Return _t
        End Get
        Set(value As Transation)
            _t = value
            txtBillNo.Text = _t.BillNo
            txtSubTotal.Text = _t.SubTotal
            txtTaxAmt.Text = _t.VATAmount
            txtDiscount.Text = _t.DiscountPer
            txtDiscountAmt.Text = _t.DiscountAmount
            txtGrandTotal.Text = _t.GrandTotal
            txtCash.Text = _t.Cash
            txtChange.Text = _t.Change

        End Set
    End Property

    Private Sub txtDiscount_TextChanged(sender As Object, e As EventArgs) Handles txtDiscount.TextChanged
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetDiscountAmount()

    End Sub


    Private Sub txtTaxAmt_TextChanged(sender As Object, e As EventArgs) Handles txtTaxAmt.TextChanged
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetGrandTotal()
    End Sub

    Private Sub txtGrandTotal_TextChanged(sender As Object, e As EventArgs) Handles txtGrandTotal.TextChanged
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetChangeAmount()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Try
            If _t.Change < 0 Then
                If MessageBox.Show("Do You Want To Print Customer Bill?", "Alert", MessageBoxButtons.YesNo) = MsgBoxResult.Yes Then
                    PrintBill()
                Else
                    If MessageBox.Show("Do You Want To Save Credit Sales?", "Alert", MessageBoxButtons.YesNo) = MsgBoxResult.Yes Then
                        Dim frm As CustomerForm = New CustomerForm()
                        frm.ShowDialog()
                        If frm.flag = 1 Then
                            'get customer id from customer form to save in invoice_info
                            _t.CustomerID = frm.customer.CustomerId
                            Me.CustomerName = frm.customer.CustomerName

                            _t.Remarks = "due"
                            If MessageBox.Show("do you want to print credit bill?", "alert", MessageBoxButtons.YesNo) = MsgBoxResult.Yes Then
                                salesType = "credit"
                                PrintBill()
                            End If
                            RecordPayment()
                        Else
                            Exit Sub
                        End If

                    Else
                        Return
                    End If

                End If

            Else
                PrintBill()
                RecordPayment()
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        

    End Sub
    Sub PrintBill()
        'PrintPreviewDialog1.ShowDialog()
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        'printing with new method
        count = Me.ListView1.Items.Count
        Try
            If count > 0 Then
                'check first page and print header part
                'and return height of header
                If _currentPrintingRow = 0 Then
                    Dim h As PrintHeader = New PrintHeader()
                   
                    h.PanNo = "Pan no.: 600945970"
                    h.Contact = "Contact : 01-4381136"
                    h.BillDate = "Date: " & Date.Now()
                    h.BillNo = "Bill No.: " & _t.BillNo

                    h.TableNo = "T:" & txtTableNo.Text
                    h.InvoiceType = "Invoice"

                    Me.header_height = h.Print(sender, e)

                    Me.body = New PrintBody()

                    'important part
                    body.row = count
                    body.Count = Decimal.Ceiling(body.row / 60)
                    body.header_height = header_height
                    body.ColNum = 5
                    'body part data for print
                    body.PrintData = PrintFun.GetPrintingDataFromList(Me.ListView1)

                End If

                body.currentPrintingRow = _currentPrintingRow

                body.Print(e)

                'check if it is last page. then print footer part
                'if not configure middle part
                If _currentPrintingRow = body.Count - 1 Then
                    Dim f As PrintFooter = New PrintFooter()
                    f.startY = body.y
                    f.SubTotal = "SubTotal : " & _t.SubTotal
                    
                    If Not Double.Parse(_t.GrandTotal) = Double.Parse(_t.SubTotal) Then
                        f.GrandTotal = "GT : " & _t.GrandTotal
                    End If


                    If Not Integer.Parse(_t.Cash) <= 0 Then
                        f.Cash = "Tender :" & _t.Cash
                    End If

                    If Not _t.DiscountAmount = Nothing Then

                        If Double.Parse(_t.DiscountAmount) > 0.0 Then
                            f.DiscountAmount = "Discount : " & _t.DiscountAmount
                        End If

                    End If

                    If Double.Parse(_t.VATAmount) > 0.0 Then
                        f.VATAmount = "TAX : " & _t.VATAmount
                    End If

                    f.WaiterName = "By : " & txtWaiter.Text

                    If salesType = "credit" Then
                        f.CustomerName = "Customer Name: " & CustomerName
                        f.ContactNo = "Customer ID: " & _t.CustomerID
                        f.Change = "Due Amount: " & _t.Change
                        f.FooterMessage = "Please Pay Due Amount At Time. ThankYou! "

                    Else
                        If Not _t.Change = Nothing Then
                            If Double.Parse(_t.Change) > 0.0 Then
                                f.Change = "Change : " & _t.Change
                            End If

                        End If
                        f.FooterMessage = "Thank You! Visit Again"
                    End If

                    f.Print(sender, e)

                    e.HasMorePages = False
                Else
                    body.x = 10
                    body.y = 10
                    e.HasMorePages = True
                    _currentPrintingRow += 1
                End If
            Else
                MessageBox.Show("There is no data to print")
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try



    End Sub
    ''' <summary>Create Invoice_Info and if Returns True load orders list in listview and tables of Table Form to refresh new data</summary>
    Sub RecordPayment()
        Try
            If _t.Create() Then
                If Bill.MakePayment(_t.BillNo) Then
                    'load orderlist in table form
                    Dim t As TablePresenter = New TablePresenter(_table)
                    t.LoadOrders()
                    t.LoadTables()
                    Me.Close()
                Else
                    MessageBox.Show("Something Went Wrong! Contact 9843408105", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try


    End Sub

    Public Property _currentPrintingRow As Integer Implements IFastPrinting._currentPrintingRow

    Public Property body As PrintBody Implements IFastPrinting.body

    Public Property count As Integer Implements IFastPrinting.count

    Public Property header_height As Integer Implements IFastPrinting.header_height

    Public Property row As Integer Implements IFastPrinting.row

    Public Property x As Integer Implements IFastPrinting.x

    Public Property y As Integer Implements IFastPrinting.y

    Private Sub chkServiceCharge_CheckedChanged(sender As Object, e As EventArgs) Handles chkServiceCharge.CheckedChanged
        Dim p As PayBillPresenter = New PayBillPresenter(Me, transaction)
        p.SetServiceChargeAmount()
    End Sub
End Class