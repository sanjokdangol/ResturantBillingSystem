﻿Public Class CreditSales

    Private Sub CreditSales_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As CreditBillPresenter = New CreditBillPresenter(Me)
        p.LoadData()
        txtCreditSales.Text = Calculate.CalculateByIndex(DataGridView1, 3)
    End Sub

    Private Sub TextBox2_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Find.FindInGridView(DataGridView1, ComboBox1.Text, txtSearch.Text)
    End Sub
End Class