﻿Public Class PaidBillForm

    Private Sub PaidBillForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As PaidBillPresenter = New PaidBillPresenter(Me)
        p.LoadPaidBillList()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim x As Export = New Export()
        x.ExportToExcel(DataGridView1)
    End Sub
End Class