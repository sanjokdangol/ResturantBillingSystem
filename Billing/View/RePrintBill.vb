﻿Public Class RePrintBill
    Implements IFastPrinting

    Property _t As OverAllTransactionReportForm
    Sub New(ByVal frm As Form)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _t = frm
    End Sub
    Private Sub RePrintBill_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As RePrintBillPresenter = New RePrintBillPresenter(Me)
        p.LoadForm()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
 
        'printing with new method
        count = Me.ListView1.Items.Count
        Try
            If count > 0 Then
                'check first page and print header part
                'and return height of header
                If _currentPrintingRow = 0 Then
                    Dim h As PrintHeader = New PrintHeader()

                    h.PanNo = "Pan no.: 600945970"
                    h.Contact = "Contact : 01-4381136"
                    h.BillDate = "Date: " & Date.Now()
                    h.BillNo = "Bill No.: " & _t.BillNo

                    h.TableNo = "T:" & txtTable.Text
                    h.InvoiceType = "Duplicate Invoice"

                    Me.header_height = h.Print(sender, e)

                    Me.body = New PrintBody()

                    'important part
                    body.row = count
                    body.Count = Decimal.Ceiling(body.row / 60)
                    body.header_height = header_height
                    body.ColNum = 5
                    'body part data for print
                    body.PrintData = PrintFun.GetPrintingDataFromList(Me.ListView1)

                End If

                body.currentPrintingRow = _currentPrintingRow

                body.Print(e)

                'check if it is last page. then print footer part
                'if not configure middle part
                If _currentPrintingRow = body.Count - 1 Then
                    Dim f As PrintFooter = New PrintFooter()
                    f.startY = body.y
                    f.SubTotal = "SubTotal : " & txtSubTotal.Text

                    If Not Convert.ToInt32(txtSubTotal.Text) = Convert.ToInt32(txtSubTotal.Text) Then
                        f.GrandTotal = "GT : " & txtGrandTotal.Text
                    End If

                    If Not Integer.Parse(txtCash.Text) = 0 Then
                        f.Cash = "Tender :" & txtCash.Text
                    End If

                    If Not Integer.Parse(txtDiscountAmt.Text) = 0 Then
                        f.DiscountAmount = "Discount : " & txtDiscountAmt.Text
                    End If

                    If Not Integer.Parse(txtTaxAmt.Text) = 0 Then
                        f.VATAmount = "TAX : " & txtTaxAmt.Text
                    End If


                    f.WaiterName = "By : " & txtWaiter.Text

                    If Not Integer.Parse(txtChange.Text) <= 0 Then
                        f.Change = "Change : " & txtChange.Text
                    End If
                    f.FooterMessage = "Thank You! Visit Again"

                    f.Print(sender, e)

                    e.HasMorePages = False
                Else
                    body.x = 10
                    body.y = 10
                    e.HasMorePages = True
                    _currentPrintingRow += 1
                End If
            Else
                MessageBox.Show("There is no data to print")
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        PrintPreviewDialog1.Document = PrintDocument1
        PrintPreviewDialog1.ShowDialog()
        Dim printDialog As PrintDialog = New PrintDialog()
        printDialog.Document = PrintDocument1
        PrintDocument1.Print()
    End Sub

    Public Property _currentPrintingRow As Integer Implements IFastPrinting._currentPrintingRow

    Public Property body As PrintBody Implements IFastPrinting.body

    Public Property count As Integer Implements IFastPrinting.count

    Public Property header_height As Integer Implements IFastPrinting.header_height

    Public Property row As Integer Implements IFastPrinting.row

    Public Property x As Integer Implements IFastPrinting.x

    Public Property y As Integer Implements IFastPrinting.y
End Class