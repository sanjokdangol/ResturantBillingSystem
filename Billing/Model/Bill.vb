﻿Imports System.Data.SqlClient
Public Class Bill
    Property Id As Integer
    Property status As Integer
    Property BillNo As Integer

    Shared _table As String = "Bill"

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Shared conString As String = ConnectionString.GetConnectionString

    Public Shared Function LockBill(ByVal BillNo) As Boolean
        Dim q As String
        Dim cmd As SqlCommand
        Dim con As SqlConnection
        Dim result = False
        q = "insert into " & _table & " (BillNo, BillStatus) values(@Id, @status)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@Id", BillNo)
            cmd.Parameters.AddWithValue("@status", 0)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Order" & ex.ToString)
        End Try

        Return result

    End Function

    Public Shared Function MakePayment(ByVal BillId As String) As Boolean
        Dim result = False
        Dim q As String
        Dim cmd As SqlCommand
        Dim con As SqlConnection
        q = "update " & _table & " set BillStatus=@status where BillNo=@BillId"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@BillId", BillId)
            cmd.Parameters.AddWithValue("@status", 1)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Order" & ex.ToString)
        End Try

        Return result

    End Function

    Public Shared Function CancelBill(ByVal BillNo) As Boolean
        Dim q As String
        Dim cmd As SqlCommand
        Dim con As SqlConnection
        Dim result = False
        q = "Update " & _table & " set BillStatus= @status where BillNo=@Id"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@Id", BillNo)
            cmd.Parameters.AddWithValue("@status", 2)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Order" & ex.ToString)
        End Try

        Return result

    End Function

End Class
