﻿' ***********************************************************************
' Assembly         : ResturantBillingSystem
' Author           : sanjok
' Created          : 03-05-2018
'
' Last Modified By : sanjok
' Last Modified On : 03-26-2018
' ***********************************************************************
' <copyright file="BillOrder.vb" company="">
'     Copyright ©  2018
' </copyright>
' <summary>

'</summary>
' ***********************************************************************
Imports System.Data.SqlClient

Public Class BillOrder
    Property OrderId As String
    Property ProductID As String
    Property WaiterID As String
    Property WaiterName As String
    Property status As String
    Property Quantity As String
    Property Total As String
    Property CategoryID As String
    Property Created_at As Date
    Property HotelTableID As Integer
    Property BillID As String

    Public Shared TableNo As String

    Dim _table As String = "BillOrder"

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString

    Public Function Create() As Boolean
        Dim result = False
        q = "insert into " & _table & " (ProductID, WaiterID, Quantity, Total, status, HotelTableID, BillID, Created_At) values(@PID, @WID,@Qty, @Total, @status, @HID, @BID, @Created_At)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@PID", Me.ProductID)
            cmd.Parameters.AddWithValue("@WID", Me.WaiterID)
            cmd.Parameters.AddWithValue("@Qty", Me.Quantity)
            cmd.Parameters.AddWithValue("@Total", Me.Total)
            cmd.Parameters.AddWithValue("@HID", Me.HotelTableID)
            cmd.Parameters.AddWithValue("@BID", Me.BillID)
            cmd.Parameters.AddWithValue("@status", 0)
            cmd.Parameters.AddWithValue("@Created_At", Date.Now())

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Order" & ex.ToString)
        End Try

        Return result

    End Function

    Public Function Update() As Boolean
        Dim result As Boolean = False
        q = "UPDATE " & _table & " SET HotelTableID=@HotelTableID, ProductID=@ProductID, WaiterID=@WaiterID, Quantity=@Quantity,Total=@Total, status=@status WHERE BillID = @BillID"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@ProductID", ProductID)
            cmd.Parameters.AddWithValue("@WaiterID", WaiterID)
            cmd.Parameters.AddWithValue("@Quantity", Quantity)
            cmd.Parameters.AddWithValue("@Total", Total)
            cmd.Parameters.AddWithValue("@status", status)
            cmd.Parameters.AddWithValue("@BillID", BillID)
            cmd.Parameters.AddWithValue("@HotelTableID", HotelTableID)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Public Function Delete() As Boolean
        Dim result As Boolean = False
        q = "DELETE FROM " & _table & " WHERE BillID = @BillID"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@BillID", Me.BillID)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Public Function GetPaidBillList() As DataTable
        Dim reader As SqlDataReader = Nothing
        q = "select BillID, HotelTableID as 'Table', WaiterName as 'Waiter', sum(Total) as 'Total' from " & _table & " as o " +
                   "join Waiter as w on w.WaiterId = o.WaiterID " +
                   "join Bill as b on b.BillNo = o.BillID " +
                   "where cast(Created_At as Date) = cast(getdate() as Date) and b.BillStatus = 1 " +
                   "group By BillID,HotelTableID, WaiterName  order By BillID DESC"


        Return GetDataTableByQuery(q)

    End Function

    Public Function GetOrderList() As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        q = "select BillID, HotelTableID as 'Table', WaiterName as 'Waiter', sum(Total) as 'Total' from " & _table & " as o " +
                   "join Waiter as w on w.WaiterId = o.WaiterID " +
                   "join Bill as b on b.BillNo = o.BillID " +
                   "where b.BillStatus = 0 " +
                   "group By BillID,HotelTableID, WaiterName  order By BillID DESC"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            reader = cmd.ExecuteReader()

        Catch ex As Exception
            Console.WriteLine("Error at GetOrderList" & ex.ToString)
        End Try
                
        Return reader

    End Function

    Public Function GetCancelledOrderList(ByVal fromDate As String, ByVal toDate As String) As DataTable
       
        Dim fDate, tDate As Date
        Date.TryParse(fromDate, fDate)
        Date.TryParse(toDate, tDate)
        If fDate = tDate Then

            q = "select Created_At as Date, BillID, HotelTableID as 'Table', WaiterName as 'Waiter', sum(Total) as 'Total' from " & _table & " as o " +
                  "join Waiter as w on w.WaiterId = o.WaiterID " +
                  "join Bill as b on b.BillNo = o.BillID " +
                  "where b.BillStatus = 2 and  cast(Created_At as Date) = cast(@toDate  as Date) " +
                  "group By BillID,HotelTableID, WaiterName, Created_At  order By BillID DESC"
        Else
            q = "select Created_At as Date, BillID, HotelTableID as 'Table', WaiterName as 'Waiter', sum(Total) as 'Total' from " & _table & " as o " +
                   "join Waiter as w on w.WaiterId = o.WaiterID " +
                   "join Bill as b on b.BillNo = o.BillID " +
                   "where b.BillStatus = 2 and cast(Created_At as Date) >= cast(@fromDate as Date) and cast(Created_At as Date) <= cast(@toDate  as Date) " +
                   "group By BillID,HotelTableID, WaiterName, Created_At  order By BillID DESC"
        End If
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@fromDate", fDate)
            cmd.Parameters.AddWithValue("@toDate", tDate)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at GetCancelledOrderList " & ex.ToString)
        End Try
        Return dt
    End Function

    Public Function GetOrderListByBillNo(ByVal billNo As String) As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        q = "select ProductName as Items, Price as Rate, Quantity as Qty, Total from " & _table & " as o" +
                   " join Product as p on p.ProductId = o.ProductID" +
                   "  where BillID=@billNo group By BillID, ProductName, Price, Quantity, Total order By ProductName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("billNo", billNo)
            reader = cmd.ExecuteReader()

        Catch ex As Exception
            Console.WriteLine("Error at GetOrderList" & ex.ToString)
        End Try
        Return reader
    End Function

    Public Function GetOrderForReprintByBillNo(ByVal billNo As String) As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        q = "select ProductName as Items, Price as Rate, Quantity as Qty, Total, WaiterID, HotelTableID from " & _table & " as o" +
                   " join Product as p on p.ProductId = o.ProductID" +
                   "  where BillID=@billNo group By BillID, ProductName, Price, Quantity, Total, WaiterID, HotelTableID order By ProductName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("billNo", billNo)
            reader = cmd.ExecuteReader()

        Catch ex As Exception
            Console.WriteLine("Error at GetOrderList" & ex.ToString)
        End Try
        Return reader
    End Function

    Public Function GetOrderByBillNo(ByVal billNo As String) As DataTable

        q = "select ProductName as Items, Price as Rate, Quantity as Qty, Total from " & _table & " as o" +
                   " join Product as p on p.ProductId = o.ProductID" +
                   "  where BillID=@billNo group By BillID, ProductName, Price, Quantity, Total order By ProductName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("billNo", billNo)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)

        Catch ex As Exception
            Console.WriteLine("Error at GetOrderList" & ex.ToString)
        End Try
        Return dt
    End Function

    Public Function Edit(ByVal billNo As String) As DataTable

        q = "select OrderId, o.ProductID As ID, ProductName as Items, Price as Rate, Quantity as Qty, Total from " & _table & " as o" +
                   " join Product as p on p.ProductId = o.ProductID" +
                   "  where BillID=@billNo group By BillID, o.ProductID ,ProductName, Price, Quantity, Total, OrderId order By OrderId Desc"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("billNo", billNo)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)

        Catch ex As Exception
            Console.WriteLine("Error at Edit" & ex.ToString)
        End Try
        Return dt
    End Function

    Public Function IsBillExist(ByVal BillID As Integer) As Boolean
        Dim res As Boolean = False
        q = "select count(BillID) from BillOrder Where BillID=@BillID"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("BillID", BillID)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsBillNoExist" & ex.ToString)
        End Try
        Return res
    End Function

    ''' <summary>Check if bill already created in Bill Table. If exist return true else return false</summary>
    Public Function IsBillExist() As Boolean
        Dim res As Boolean = False
        q = "select count(BillID) from BillOrder Where BillID=@BillID"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("BillID", BillID)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsBillNoExist" & ex.ToString)
        End Try
        Return res
    End Function

    Public Function GetMaxBillNo() As String
        Dim BillNo As String = Nothing
        q = "select max(BillID) from BillOrder"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                BillNo = reader(0)
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at GetMaxBillNo" & ex.ToString)
        End Try
        Return BillNo
    End Function

    

    Public Function GetDaySalesList() As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        q = "select ProductName as Food_Items, Price, sum(Quantity) as Qty, sum(Total) as Total from " & _table & " as b " +
            "inner join Product as p on p.ProductId = b.ProductID " +
            "join Bill as i on i.BillNo = b.BillID " +
            "where cast(Created_At as Date) = cast(getdate() as Date) and i.BillStatus = 1" +
            "group by ProductName, Price " +
            "order by Qty DESC"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            reader = cmd.ExecuteReader()

        Catch ex As Exception
            Console.WriteLine("Error at GetOrderList" & ex.ToString)
        End Try

        Return reader

    End Function

    Public Function GetDaySalesReaderByDate(ByVal fromDate As String, ByVal toDate As String) As SqlDataReader
        Dim fDate, tDate As Date
        Date.TryParse(fromDate, fDate)
        Date.TryParse(toDate, tDate)
        Dim reader As SqlDataReader = Nothing
        q = "select ProductName as Food_Items,Price, sum(Quantity) as Qty, sum(Total) as Total from " & _table & " as b " +
           "inner join Product as p on p.ProductId = b.ProductID " +
           "join Bill as i on i.BillNo = b.BillID " +
           "where cast(Created_At as Date) >= cast(@fromDate as Date) and cast(Created_At as Date) <= cast(@toDate  as Date) and i.BillStatus = 1" +
           "group by ProductName, Price " +
           "order by Qty DESC"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("fromDate", fDate)
            cmd.Parameters.AddWithValue("toDate", tDate)
            reader = cmd.ExecuteReader()

        Catch ex As Exception
            Console.WriteLine("Error at GetDaySalesReader " & ex.ToString)
        End Try
        Return reader
    End Function

    Public Function GetDataTableByQuery(ByVal q As String) As DataTable
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at GetDataTableByQuery " & ex.ToString)
        End Try

        Return dt
    End Function


    Public Function GetOrderListByDate(ByVal fromDate As String, ByVal toDate As String) As DataTable
        Dim fDate, tDate As Date
        Date.TryParse(fromDate, fDate)
        Date.TryParse(toDate, tDate)
        If fDate = tDate Then
            q = "select ProductName as Food_Items, Price, sum(Quantity) as Qty, sum(Total) as Total from " & _table & " as b " +
           "inner join Product as p on p.ProductId = b.ProductID " +
           "join Bill as i on i.BillNo = b.BillID " +
           "where cast(Created_At as Date) = cast(@toDate  as Date)" +
           "group by ProductName, Price " +
           "order by Qty DESC"
        Else
            q = "select ProductName as Food_Items, Price, sum(Quantity) as Qty, sum(Total) as Total from " & _table & " as b " +
           "inner join Product as p on p.ProductId = b.ProductID " +
           "join Bill as i on i.BillNo = b.BillID " +
           "where cast(Created_At as Date) >= cast(@fromDate as Date) and cast(Created_At as Date) <= cast(@toDate  as Date)" +
           "group by ProductName, Price " +
           "order by Qty DESC"
        End If
        
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("fromDate", fDate)
            cmd.Parameters.AddWithValue("toDate", tDate)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at GetDataTableByQuery " & ex.ToString)
        End Try
        Return dt
    End Function

    Public Function GetDaySales() As DataTable

        q = "select ProductName as Food_Items, Price, sum(Quantity) as Qty, sum(Total) as Total from " & _table & " as b " +
            "inner join Product as p on p.ProductId = b.ProductID " +
            "join Bill as i on i.BillNo = b.BillID " +
            "where cast(Created_At as Date) = cast(getdate() as Date) and BillStatus =1 " +
            "group by ProductName, Price " +
            "order by Qty DESC"

        Return GetDataTableByQuery(q)

    End Function

    Function SearchProduct(ByVal text As String) As SqlDataReader
        Dim reader As SqlDataReader = Nothing
        q = "select ProductId, ProductName from Product where ProductName like '@name'"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", text)
            reader = cmd.ExecuteReader()

        Catch ex As Exception
            Console.WriteLine("Error " & ex.ToString)
        End Try

        Return reader
    End Function
End Class
