﻿Imports System.Data.SqlClient
Public Class PayBillPresenter
    Dim _table As TableForm
    Dim _pbv As PayBillForm
    Dim _t As Transation
    Dim cal As Calculator = New Calculator()
    Sub New(ByVal view As Form)
        _table = view
    End Sub

    Sub New(ByVal view As Form, ByVal transaction As Transation)
        _pbv = view
        _t = transaction
    End Sub

    Sub New(ByVal view As PayBillForm, ByVal table As TableForm, ByVal transaction As Transation)
        _pbv = view
        _table = table
        _t = transaction
    End Sub

    Sub InitValues()
        _pbv.txtBillNo.Text = 0
        _pbv.txtSubTotal.Text = 0
        _pbv.txtTaxAmt.Text = 0
        _pbv.txtDiscount.Text = 0
        _pbv.txtDiscountAmt.Text = 0
        _pbv.txtGrandTotal.Text = 0
        _pbv.txtCash.Text = ""
        _pbv.txtChange.Text = 0
    End Sub

    Public Sub ReadDataFromSelection()
        Dim billNo As String = ""
        Dim tableNo As String = ""
        Dim total As String = ""
        Dim waiter As String = ""
        If _table.ListView1.SelectedItems.Count > 0 Then
            Dim data As ListView.SelectedListViewItemCollection = _table.ListView1.SelectedItems
            Dim item As ListViewItem
            For Each item In data
                billNo = item.SubItems(0).Text
                tableNo = item.SubItems(1).Text
                waiter = item.SubItems(2).Text
                total = item.SubItems(3).Text
            Next


            Dim ar() As String = {billNo, tableNo, total, waiter}

            Dim frm As PayBillForm = New PayBillForm(ar, _table)

            frm.Show()

        End If
    End Sub

    Public Sub PayBill()
        ReadDataFromSelection()
    End Sub

    'calculate tax amount and grand total when discount
    Public Sub SetDiscountAmount()
        If _t.DiscountPer = "" Then
            Return
        End If

        _pbv.txtDiscountAmt.Text = cal.CalculateDiscount(_t.SubTotal, _pbv.txtDiscount.Text)
        _t.DiscountAmount = _pbv.txtDiscountAmt.Text
        SetTaxAmount()

    End Sub

    Public Sub SetGrandTotal()

        If _t.VATAmount = "" Then
            _t.VATAmount = 0

        End If
        If _t.DiscountAmount = "" Then
            _pbv.txtDiscountAmt.Text = 0
            _t.DiscountAmount = 0

        End If
        _pbv.txtGrandTotal.Text = cal.CalculateTotal(_t.SubTotal, _t.VATAmount, _t.DiscountAmount)

    End Sub

    Public Sub SetChangeAmount()
        If _t.Cash = "" Then
            _t.Cash = 0
        End If
        _pbv.txtChange.Text = cal.CalculateChange(_t.GrandTotal, _t.Cash)
        _t.Change = _pbv.txtChange.Text
    End Sub

    Public Sub SetTaxAmount()
        _pbv.txtTaxAmt.Text = 0
        If (_pbv.chkTax.Checked) Then
            _pbv.txtTaxAmt.Text = 0.13 * _t.GrandTotal
            _t.VATAmount = _pbv.txtTaxAmt.Text

        End If
        SetGrandTotal()
    End Sub

    Public Sub SetServiceChargeAmount()
        _pbv.txtServiceCharge.Text = 0
        If (_pbv.chkServiceCharge.Checked) Then
            _pbv.txtServiceCharge.Text = Transation.ServiceChargePer * _t.GrandTotal
            _t.ServiceCharge = _pbv.txtServiceCharge.Text

        End If
        SetGrandTotal()
    End Sub

    'get detail list from listview from printform
    Public Function GetListData() As String(,)
        Dim items(,) As String
        Dim row As Integer = 0
        row = _pbv.ListView1.Items.Count
        Dim i, j As Integer
        items = New String(row - 1, 4) {}
        If (row > 0) Then
            i = 0
            While i < row
                j = 0
                While j < 5
                    items(i, j) = _pbv.ListView1.Items(i).SubItems(j).Text
                    j = j + 1
                End While
                i = i + 1
            End While
        End If
        Return items
    End Function


    Public Sub PrintBill(sender, e)
        Dim p As PrintBill = New PrintBill(_pbv, _t)
        p.PrintPage(sender, e, GetListData(), _pbv.ListView1.Items.Count)
        If Bill.MakePayment(_t.BillNo) Then
            'load orderlist in table form after bill has been paid
            Dim t As TablePresenter = New TablePresenter(_table)
            t.LoadOrders()

            _pbv.Close()
        Else
            MessageBox.Show("Something Went Wrong! Contact 9843408105", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        _t.Create()
    End Sub
End Class
