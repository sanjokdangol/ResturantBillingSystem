﻿Imports System.Data.SqlClient
Public Class OrderPresenter
    Dim _view As OrderForm
    Property order As BillOrder = New BillOrder()

    Property product As Product

    Dim ProductName As String

    Sub New()

    End Sub

    Sub New(ByVal view As Form)
        _view = view

    End Sub

    Sub New(ByVal view As Form, ByVal model As BillOrder)
        _view = view
        order = model

    End Sub

    Sub New(ByVal view As Form, ByVal model As Product)
        _view = view
        product = model

    End Sub

    Sub New(ByVal view As Form, ByVal model As Product, ByVal order As BillOrder)
        _view = view
        product = model
        order = order

    End Sub
  

    Sub Reset()
        _view.txtPrice.Text = "0"
        _view.NumericUpDown1.Value = 1

        ResetCategory()
        ResetProduct()
    End Sub

    Sub ResetProduct()
        _view.cboProduct.SelectedIndex = -1
    End Sub

    Sub ResetCategory()
        _view.cboCategory.SelectedIndex = -1
    End Sub

    Sub ResetGrandTotal()
        _view.txtGrandTotal.Text = 0
    End Sub

    Sub SearchProduct()

        _view.cboProduct.AutoCompleteMode = AutoCompleteMode.Suggest
        _view.cboProduct.AutoCompleteSource = AutoCompleteSource.CustomSource
        Dim combData As AutoCompleteStringCollection = New AutoCompleteStringCollection()
        Dim pr As New Product()
        pr.SearchProduct(combData)
        _view.cboProduct.AutoCompleteCustomSource = combData
        Try
            If Not _view.cboProduct.Text = "System.Data.DataRowView" And Not _view.cboProduct.Text = Nothing Then

                product.ProductName = _view.cboProduct.Text
                product.ProductId = product.GetProductID(product.ProductName)
                SetProductPrice()
                order.Quantity = _view.NumericUpDown1.Text

                order.Total = _view.txtSubTotal.Text
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub LoadForm()
        Try

            order.BillID = _view.BillID
            If order.BillID > 0 Or Not order.BillID = Nothing Then
                If order.IsBillExist(order.BillID) Then

                    EditForm()
                    _view.txtTableNumber.Enabled = True
                Else
                    MessageBox.Show("Sorry Something Went Wrong! BillNo Does not Exist")
                End If

            Else
                NewForm()
                _view.txtTableNumber.Enabled = False
            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub NewForm()
        Try
            '_view.txtTableNumber.Text = _view._table.txtTblNo.Text
            _view.txtDate.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            LoadBillNo()
            LoadCategoryInCombo()
            LoadWaiterList()
            Reset()

        Catch ex As Exception
            Console.WriteLine("Error at LoadForm" & ex.ToString)
        End Try
    End Sub

    Public Sub EditForm()

        _view.txtTableNumber.Text = _view.TblNo
        _view.txtBill.Text = _view.BillID
        _view.cboWaiter.SelectedText = _view.bill.WaiterName



        order.HotelTableID = _view.txtTableNumber.Text

        _view.cboWaiter.SelectedValue = order.WaiterID
        Dim dt As DataTable = New DataTable()
        dt = order.Edit(order.BillID)

        With _view.lvBill
            .View = View.Details
            .GridLines = True
            .Columns.Clear()
            .Items.Clear()
        End With


        LoadDataTable(dt, _view.lvBill)
        SetGrandTotal()
        SetTotalItems()

        LoadCategoryInCombo()


    End Sub

    Sub SetGrandTotal()
        _view.txtGrandTotal.Text = GetGrandTotal(_view.lvBill)
    End Sub

    Sub LoadDataTable(ByVal dtable As DataTable, ByVal listView1 As ListView)
        ' Clear the ListView control
        listView1.Items.Clear()

        Dim ColCount As Integer = dtable.Columns.Count
        'Add columns
        Dim k As Integer = 0

        While k < ColCount
            listView1.Columns.Add(dtable.Columns(k).ColumnName)
            If k = 2 Then
                'listView1.Columns(k).AutoResize(ColumnHeaderAutoResizeStyle.None)
                listView1.Columns(k).Width = 600
                'listView1.Columns(k).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            End If

            k += 1
        End While
        ' Display items in the ListView control
        Dim i As Integer = 0
        While i < dtable.Rows.Count
            Dim drow As DataRow = dtable.Rows(i)

            ' Only row that have not been deleted
            If drow.RowState <> DataRowState.Deleted Then
                ' Define the list items
                Dim lvi As ListViewItem = New ListViewItem(drow(0).ToString())
                Dim j As Integer = 1
                While j < ColCount
                    lvi.SubItems.Add(drow(j).ToString())
                    j += 1
                End While
                ' Add the list items to the ListView
                listView1.Items.Add(lvi)
            End If
            i += 1
        End While

        'listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
        'listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

    End Sub

    'Load Bill number
    Sub LoadBillNo()
        Dim order As New BillOrder()
        Dim billNo As String = order.GetMaxBillNo()
        If billNo Is Nothing Then
            _view.txtBill.Text = 1
        Else
            _view.txtBill.Text = billNo + 1
        End If
    End Sub

    'load category of product while loading form
    Sub LoadCategoryInCombo()

        'Dim dt As DataTable = New DataTable()
        Dim ds As DataSet = New DataSet()
        Dim _category As CategoryPresenter = New CategoryPresenter()

        ds = _category.GetCategoryDataSet()

        'Dim items = dt.AsEnumerable().Select(Function(d) DirectCast(d(1).ToString(), Object)).ToArray()
        '_view.cboCategory.Items.AddRange(items)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                _view.cboCategory.DataSource = ds.Tables(0).DefaultView
                _view.cboCategory.DisplayMember = "CategoryName"
                _view.cboCategory.ValueMember = "Id"
                '_view.cboCategory.DisplayMember = dt.Columns("CategoryName").ToString
                '_view.cboCategory.ValueMember = dt.Columns("Id").ToString
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub



    'load product when category is selected
    Sub LoadProductInComboByCategory()
        '_view.cboCategory.SelectedValue datarowview is send empty at first time loading form and send error so find solution to solve it

        Try
            Dim categoryId As String
            Dim p As Product = New Product()
            Dim dt As DataTable = New DataTable()

            If Not _view.cboCategory.SelectedValue.ToString = "System.Data.DataRowView" Then
                If _view.cboCategory.SelectedValue.ToString Then
                    categoryId = _view.cboCategory.SelectedValue.ToString
                Else
                    categoryId = DirectCast(_view.cboCategory.SelectedValue, DataRowView).Item("Id")
                End If

                dt = p.GetProductByCategory(categoryId)
                _view.cboProduct.DataSource = dt
                _view.cboProduct.DisplayMember = dt.Columns("ProductName").ToString
                _view.cboProduct.ValueMember = Integer.Parse(dt.Columns("ProductId").ToString)
            End If

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try


    End Sub


    Sub SetTotalItems()
        _view.txtItems.Text = GetTotalItems().ToString()
    End Sub

    Public Function RemoveBtn(ByVal i As Integer) As Button
        Dim btn As Button = New Button()

        btn.Text = "+"
        btn.Name = i

        btn.BackColor = Color.Red

        'AddHandler btn.Click, AddressOf ClickHandler
        Dim p As Point = _view.lvBill.Items(0).Position

        p.X += 500
        p.Y += 20

        btn.Location = p
        btn.Width = 40


        Return btn
    End Function


    Public Sub IncreaseQty()
        Try
            If _view.lvBill.SelectedItems.Count > 0 Then
                _view.lvBill.SelectedItems(0).SubItems(4).Text += 1
                _view.lvBill.SelectedItems(0).SubItems(5).Text = CalculateSubTotal(_view.lvBill.SelectedItems(0).SubItems(3).Text, _view.lvBill.SelectedItems(0).SubItems(4).Text)
                _view.txtGrandTotal.Text = GetGrandTotal(_view.lvBill)
            Else
                MessageBox.Show("First Select Item", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception

        End Try


    End Sub

    Public Sub DecreaseQty()
        Try
            If _view.lvBill.SelectedItems.Count > 0 Then

                If _view.lvBill.SelectedItems(0).SubItems(4).Text < 2 Then
                    Return
                End If
                _view.lvBill.SelectedItems(0).SubItems(4).Text -= 1
                _view.lvBill.SelectedItems(0).SubItems(5).Text = CalculateSubTotal(_view.lvBill.SelectedItems(0).SubItems(3).Text, _view.lvBill.SelectedItems(0).SubItems(4).Text)
                _view.txtGrandTotal.Text = GetGrandTotal(_view.lvBill)
            Else
                MessageBox.Show("First Select Item", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub

    Sub AddProductToBill()
        Dim lv As ListView = _view.lvBill
        product.ProductName = _view.cboProduct.Text

        If (product.ProductName Is Nothing Or product.ProductName = "") Then
            MessageBox.Show("Choose Item", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            _view.cboProduct.Focus()
            Return
        End If

        Try


            product.ProductId = product.GetProductID(product.ProductName)
            SetProductPrice()
            product.Price = _view.txtPrice.Text
            order.Total = _view.txtSubTotal.Text
            order.Quantity = _view.NumericUpDown1.Text
            If lv.Items.Count = 0 Then
                Dim lst As ListViewItem = New ListViewItem()

                lst.SubItems.Add(product.ProductId)
                lst.SubItems.Add(_view.cboProduct.Text)
                lst.SubItems.Add(product.Price)
                lst.SubItems.Add(order.Quantity)
                lst.SubItems.Add(CalculateSubTotal(product.Price, order.Quantity))

                lv.Items.Add(lst)
                'lv.Controls.Add(RemoveBtn(0))

                _view.txtGrandTotal.Text = GetGrandTotal(lv)
                SetTotalItems()
                Return
            End If
            Dim j As Integer = 0

            While j < lv.Items.Count
                If (lv.Items(j).SubItems(1).Text = product.ProductId) Then

                    lv.Items(j).SubItems(1).Text = product.ProductId
                    lv.Items(j).SubItems(2).Text = product.ProductName
                    lv.Items(j).SubItems(3).Text = product.Price
                    lv.Items(j).SubItems(4).Text = order.Quantity + Convert.ToInt32(lv.Items(j).SubItems(4).Text)
                    lv.Items(j).SubItems(5).Text = CalculateSubTotal(lv.Items(j).SubItems(3).Text, lv.Items(j).SubItems(4).Text)
                    _view.txtGrandTotal.Text = GetGrandTotal(lv)
                    SetTotalItems()

                    Return
                End If
                j += 1
            End While

            Dim lv1 As ListViewItem = New ListViewItem()
            lv1.SubItems.Add(product.ProductId)
            lv1.SubItems.Add(product.ProductName)
            lv1.SubItems.Add(product.Price)
            lv1.SubItems.Add(order.Quantity)
            lv1.SubItems.Add(CalculateSubTotal(product.Price, order.Quantity))
            lv.Items.Add(lv1)

            _view.txtGrandTotal.Text = GetGrandTotal(lv)
            SetTotalItems()


        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

    End Sub



    Public Function CalculateSubTotal(ByVal n1 As String, ByVal n2 As String) As String
        Dim total As String = "0"
        Try
            total = (Convert.ToInt32(n1) * Convert.ToInt32(n2)).ToString
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return total
    End Function

    'Load price of product when selecting product for subtotal calculation
    Sub SetProductPrice()

        _view.txtPrice.Text = product.GetProductPrice(product.ProductId)
        product.Price = _view.txtPrice.Text
    End Sub

    'axb while selecting product in combobox
    Sub GetSubTotal()
        _view.txtSubTotal.Text = "0"
        _view.txtSubTotal.Text = CalculateSubTotal(product.Price, _view.NumericUpDown1.Value)
    End Sub

    'Calculate grand total from listview of bill
    Public Function GetGrandTotal(ByVal lv As ListView) As Double
        Dim sum As Double = 0.0
        Dim i As Integer = 0
        Try

            While i < lv.Items.Count
                sum = sum + Convert.ToDouble(lv.Items(i).SubItems(5).Text)
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return sum
    End Function


    'return total items in listview
    Public Function GetTotalItems() As Integer
        Return _view.lvBill.Items.Count
    End Function

    'save order to db
    ''' <summary>Order Food Items form Order Form if items count &gt; 0. If bill exist in Bill Table Delete all orders and create new order list</summary>
    Public Sub OrderBill()
        Dim success As Boolean = False
        
        If ValidateFields() Then
            Try
                If _view.lvBill.Items.Count > 0 Then
                    order.BillID = _view.txtBill.Text
                    If Not _view.cboWaiter.SelectedValue = Nothing Then
                        order.WaiterID = _view.cboWaiter.SelectedValue
                    Else
                        order.WaiterID = Waiter.GetWaiterIdByName(_view.bill.WaiterName)
                    End If
                    order.HotelTableID = _view.txtTableNumber.Text

                    Dim billExist As Boolean = order.IsBillExist()
                    If billExist Then
                        order.Delete()
                        order.HotelTableID = _view.txtTableNumber.Text
                    End If

                    For Each li As ListViewItem In _view.lvBill.Items
                        order.ProductID = Integer.Parse(li.SubItems(1).Text)
                        order.Quantity = li.SubItems(4).Text
                        order.Total = Integer.Parse(li.SubItems(5).Text)
                        success = order.Create()
                    Next

                    'create bill status 0
                    'if not locked then create
                    If Not billExist Then
                        Bill.LockBill(order.BillID)
                    End If

                    'loadOrders must be after lockBill
                    Dim t As TablePresenter = New TablePresenter(_view._table)
                    t.LoadOrders()
                    t.LoadTables()

                    'This method is depreciated now check lock table from listview
                    'Table.LockTable(order.HotelTableID)
                    _view.Close()

                End If
            Catch ex As Exception
                Console.WriteLine("Error at OrderBill: " & ex.ToString)
            End Try
        End If

    End Sub

    Public Function ValidateFields() As Boolean
        Dim res As Boolean = True
        If GetTotalItems() < 1 Then
            MessageBox.Show("No Item Added To Bill", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            res = False
        End If

        If _view.cboWaiter.Text = "" Then
            MessageBox.Show("Choose Waiter", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            _view.cboWaiter.Focus()
            res = False
        End If
        Return res
    End Function

    'load waiter list in combobox
    Sub LoadWaiterList()
        Try
            Dim dt As DataTable = New DataTable()
            dt = New Waiter().Read()
            _view.cboWaiter.DataSource = dt
            _view.cboWaiter.DisplayMember = dt.Columns("WaiterName").ToString
            _view.cboWaiter.ValueMember = dt.Columns("WaiterId").ToString
        Catch ex As Exception
            Console.WriteLine("Error at LoadWaiterList" & ex.ToString)
        End Try
    End Sub


    'not used underconstruction
    Sub RemoveItemFromList()
        If (_view.lvBill.Items.Count = 0) Then
            MessageBox.Show("No items to remove", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim response As MsgBoxResult = MessageBox.Show("Are Your Sure To Cancel This Item", "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If response = MsgBoxResult.Yes Then
                Dim itmCnt As Integer = 0
                Dim i As Integer = 0
                Dim t As Integer = 0

                Try
                    _view.lvBill.FocusedItem.Remove()
                    itmCnt = _view.lvBill.Items.Count
                    t = 1

                    For i = 1 To i <= itmCnt + 1 Step 1
                        t = t + 1
                    Next
                    _view.txtSubTotal.Text = GetGrandTotal(_view.lvBill)

                    If (_view.lvBill.Items.Count = 0) Then
                        _view.txtSubTotal.Text = ""
                    End If
                Catch ex As Exception
                    Console.WriteLine("Error:RemoveItemFromList" & ex.ToString)
                End Try
            Else
                Return
            End If
            SetGrandTotal()
            SetTotalItems()

        End If
    End Sub

End Class
