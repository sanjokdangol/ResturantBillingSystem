﻿Imports System.Data.SqlClient
Public Class RePrintBillPresenter
    Inherits Print


    Dim _v As RePrintBill
    Sub New()

    End Sub

    Sub New(ByVal view As RePrintBill)
        _v = view

    End Sub

    Sub setBillID(ByVal BillID As String)
        BillNo = BillID
    End Sub

    Sub LoadForm()
        LoadOrders()
        GetTransactionDetails()
    End Sub
    Sub LoadOrders()

        Dim reader As SqlDataReader
        Try
            _v.ListView1.Items.Clear()
            Dim order As BillOrder = New BillOrder()

            reader = order.GetOrderForReprintByBillNo(_v._t.BillNo)
            Dim i As Integer = 1
            While (reader.Read())
                If _v.txtWaiter.Text = Nothing Then
                    _v.txtWaiter.Text = Waiter.GetWaiterNameById(reader(4))
                End If
                If _v.txtTable.Text = Nothing Then
                    _v.txtTable.Text = reader(5)
                End If
                _v.ListView1.Items.Add(New ListViewItem(
                            New String() {
                                i,
                reader(0).ToString(),
                reader(1).ToString(),
                reader(2).ToString(),
                reader(3).ToString()
                            }))
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine("Error: LoadOrders TablePresenter " & ex.ToString)
        End Try

    End Sub

    Sub GetTransactionDetails()
        Try
            Dim dt As DataTable
            Dim t As Transation = New Transation()
            t.BillNo = _v._t.BillNo
            dt = t.GetTransactionDetailsByBillNo()
            For Each row As DataRow In dt.Rows
                _v.txtBillNo.Text = row("BillNo")
                _v.txtCash.Text = row("Cash")
                _v.txtChange.Text = row("Change")
                _v.txtDiscountAmt.Text = row("DiscountAmount")
                _v.txtGrandTotal.Text = row("GrandTotal")
                _v.txtDiscount.Text = row("DiscountPer")
                _v.txtSubTotal.Text = row("SubTotal")
                _v.txtBillDate.Text = row("BillDate")
                _v.txtTaxAmt.Text = row("VATAmount")
            Next row
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        
    End Sub


End Class
