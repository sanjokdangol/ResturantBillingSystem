﻿Imports System.Data.SqlClient

Public Class DailySaleReportPresenter

    Dim _view As DailySalesReportForm
    Dim _model As BillOrder

    Sub New(ByVal view As Form)
        _view = view
    End Sub

    Sub New(ByVal view As Form, ByVal model As BillOrder)
        _view = view
        _model = model
    End Sub

    Sub LoadDataToGridView()
        _view.btnRefresh.Enabled = False
        _model = New BillOrder()
        _view.DataGridView1.DataSource = _model.GetDaySales()
        _view.txtTotalSales.Text = Calculate.CalculateByIndex(_view.DataGridView1, 3)
        _view.txtTotalItems.Text = Calculate.CalculateByIndex(_view.DataGridView1, 2)
    End Sub

    Sub LoadDataByDate()
        Dim fdate = DateTime.Parse(_view.fromDate.Text)
        Dim tdate = DateTime.Parse(_view.toDate.Text)

        If (tdate - fdate).Days < 0 Then
            MessageBox.Show("Choose At least Yesterday and Today", "Alert", MessageBoxButtons.OK)
            Return
        End If
        _view.btnRefresh.Enabled = True
        Dim b As BillOrder = New BillOrder()


        Dim dt As DataTable = b.GetOrderListByDate(_view.fromDate.Text, _view.toDate.Text)
        _view.DataGridView1.DataSource = dt
        _view.txtTotalItems.Text = Calculate.CalculateByIndex(_view.DataGridView1, 2)
        _view.txtTotalSales.Text = Calculate.CalculateByIndex(_view.DataGridView1, 3)
    End Sub
End Class
