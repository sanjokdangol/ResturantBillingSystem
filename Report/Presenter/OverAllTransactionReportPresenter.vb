﻿Public Class OverAllTransactionReportPresenter

    Dim _view As OverAllTransactionReportForm
    Dim _model As BillOrder

    Sub New(ByVal view As Form)
        _view = view
    End Sub

    Sub New(ByVal view As Form, ByVal model As BillOrder)
        _view = view
        _model = model
    End Sub

    Sub LoadDataToGridView()
        Dim b As Transation = New Transation()
        Dim dt As DataTable = b.Read()
        _view.DataGridView1.DataSource = dt
    
        GetCalucation()
    End Sub

    Sub LoadDataByDate()
        Dim fdate = DateTime.Parse(_view.fromDate.Text)
        Dim tdate = DateTime.Parse(_view.toDate.Text)

        If (tdate - fdate).Days < 0 Then
            MessageBox.Show("Choose At least Yesterday and Today", "Alert", MessageBoxButtons.OK)
            Return
        End If

        Dim b As Transation = New Transation()
        Dim dt As DataTable = b.GetInvoiceListByDate(_view.fromDate.Text, _view.toDate.Text)
        _view.DataGridView1.DataSource = dt
        GetCalucation()
    End Sub

    Sub GetCalucation()
        _view.txtCash.Text = Calculate.CalculateByIndex(_view.DataGridView1, 8) - Calculate.CalculatePositiveNumberByIndex(_view.DataGridView1, 9) 'cashreceived - positive change amount
        _view.txtDiscount.Text = Calculate.CalculateByIndex(_view.DataGridView1, 6)
        _view.txtTotalSales.Text = Calculate.CalculateByIndex(_view.DataGridView1, 2)
        _view.txtTotalVat.Text = Calculate.CalculateByIndex(_view.DataGridView1, 4)
        _view.txtGrandTotal.Text = Calculate.CalculateByIndex(_view.DataGridView1, 7)
        _view.txtCreditSales.Text = Calculate.CalculateNagetiveNumberByIndex(_view.DataGridView1, 9)
        _view.txtChange.Text = Calculate.CalculatePositiveNumberByIndex(_view.DataGridView1, 9)
    End Sub

    Function GetBillID() As String
        Dim billID As String = ""
        Try
            Dim dr As DataGridViewRow = _view.DataGridView1.SelectedRows(0)
            billID = dr.Cells("BillNo").Value.ToString
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return billID
    End Function
End Class
