﻿Public Class DailySalesReportForm
    Implements IFastPrinting

    Dim _currentPrintingRow As Integer

    Dim i As Integer
    Dim x As Integer
    Dim y As Integer
    Dim header_height As Integer
    Dim count As Integer
    Dim row As Integer
    Dim body As PrintBody

    Private Sub DailySalesReportForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As DailySaleReportPresenter = New DailySaleReportPresenter(Me)
        p.LoadDataToGridView()

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim p As DailySaleReportPresenter = New DailySaleReportPresenter(Me)
        p.LoadDataByDate()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim export As Export = New Export()
        export.ExportToExcel(DataGridView1)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        _currentPrintingRow = 0
        i = 0
        x = 10
        y = 40

        
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage

        'printing with new method
        count = Me.DataGridView1.Rows.Count
        Try
            If count > 0 Then
                'check first page and print header part
                'and return height of header
                If _currentPrintingRow = 0 Then
                    Dim h As PrintHeader = New PrintHeader()
                    

                    If fromDate.Text = toDate.Text Then
                        h.BillDate = "Date : " & fromDate.Text
                    Else
                        h.BillNo = "Date : " & fromDate.Text
                        h.BillDate = " To : " & toDate.Text
                    End If

                    h.InvoiceType = "Day Report"

                    Me.header_height = h.Print(sender, e)

                    Me.body = New PrintBody()


                    'important part
                    body.row = count

                    body.header_height = header_height
                    body.ColNum = 5
                    'body part data for print
                    body.PrintData = PrintFun.GetPrintingDataFromGrid(Me.DataGridView1)

                End If

                body.currentPrintingRow = _currentPrintingRow

                body.Print(e)

                'check if it is last page. then print footer part
                'if not configure middle part
                If _currentPrintingRow = body.Count - 1 Then
                    Dim f As PrintFooter = New PrintFooter()
                    f.startY = body.y

                    f.SubTotal = "Total Sales Rs.: " & txtTotalSales.Text
                    f.GrandTotal = "Total Items Sold : " & txtTotalItems.Text

                    f.FooterMessage = "Thank You! Visit Again"
                    f.Print(sender, e)

                    e.HasMorePages = False
                Else
                    body.x = 10
                    body.y = 10
                    e.HasMorePages = True
                    _currentPrintingRow += 1
                End If
            Else
                MessageBox.Show("There is no data to print")
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub


    Private Sub TextBox4_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Find.FindInGridView(DataGridView1, "Food_Items", txtSearch.Text)
    End Sub

    Public Property _currentPrintingRow1 As Integer Implements IFastPrinting._currentPrintingRow

    Public Property body1 As PrintBody Implements IFastPrinting.body

    Public Property count1 As Integer Implements IFastPrinting.count

    Public Property header_height1 As Integer Implements IFastPrinting.header_height

    Public Property row1 As Integer Implements IFastPrinting.row

    Public Property x1 As Integer Implements IFastPrinting.x

    Public Property y1 As Integer Implements IFastPrinting.y

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Dim p As DailySaleReportPresenter = New DailySaleReportPresenter(Me)
        p.LoadDataToGridView()
    End Sub


    Private Sub Button3_Click_1(sender As Object, e As EventArgs) Handles Button3.Click
        PrintPreviewDialog1.Document = PrintDocument1
        PrintPreviewDialog1.ShowDialog()
        Dim printDialog As PrintDialog = New PrintDialog()
        printDialog.Document = PrintDocument1
    End Sub
End Class