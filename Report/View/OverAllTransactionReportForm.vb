﻿Public Class OverAllTransactionReportForm
    Property BillNo As String
    Private Sub OverAllTransactionReportForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim p As OverAllTransactionReportPresenter = New OverAllTransactionReportPresenter(Me)
        p.LoadDataToGridView()
       
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim export As Export = New Export()
        export.ExportToExcel(DataGridView1)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim p As OverAllTransactionReportPresenter = New OverAllTransactionReportPresenter(Me)
        p.LoadDataByDate()
    End Sub

    Private Sub TextBox4_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged

        Find.FindIntegerInGridView(DataGridView1, "BillNo", txtSearch.Text)
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim p As OverAllTransactionReportPresenter = New OverAllTransactionReportPresenter(Me)
        p.LoadDataToGridView()
    End Sub

    Private Sub DataGridView1_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.RowHeaderMouseClick
        Dim p As OverAllTransactionReportPresenter = New OverAllTransactionReportPresenter(Me)
        Me.BillNo = p.GetBillID
        Dim r As RePrintBillPresenter = New RePrintBillPresenter()

        Dim frm As New RePrintBill(Me)
        frm.Show()
    End Sub
End Class