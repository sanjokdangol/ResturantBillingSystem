﻿Public Class CancelledOrdersForm

    Private Sub CancelledOrdersForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim o As New BillOrder()
        DataGridView1.DataSource = o.GetCancelledOrderList(fromDate.Text, toDate.Text)
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim fdate = DateTime.Parse(fromDate.Text)
        Dim tdate = DateTime.Parse(toDate.Text)

        If (tdate - fdate).Days < 0 Then
            MessageBox.Show("Choose At least Yesterday and Today", "Alert", MessageBoxButtons.OK)
            Return
        End If
        Dim o As New BillOrder()
        DataGridView1.DataSource = o.GetCancelledOrderList(fromDate.Text, toDate.Text)
    End Sub
End Class