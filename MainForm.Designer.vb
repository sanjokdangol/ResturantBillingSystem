﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BillingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnPaidBillsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaidBillToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailySalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DailyTransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CategoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WaiterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WordPadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotePadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TaskManagerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.SoftwareToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComanyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ViewToolStripMenuItem, Me.BillingToolStripMenuItem, Me.ReportsToolStripMenuItem, Me.MasterToolStripMenuItem, Me.AboutToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1237, 29)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(12, 25)
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.ViewToolStripMenuItem.Image = CType(resources.GetObject("ViewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(75, 25)
        Me.ViewToolStripMenuItem.Text = "Table"
        '
        'BillingToolStripMenuItem
        '
        Me.BillingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UnPaidBillsToolStripMenuItem, Me.PaidBillToolStripMenuItem})
        Me.BillingToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.BillingToolStripMenuItem.Image = CType(resources.GetObject("BillingToolStripMenuItem.Image"), System.Drawing.Image)
        Me.BillingToolStripMenuItem.Name = "BillingToolStripMenuItem"
        Me.BillingToolStripMenuItem.Size = New System.Drawing.Size(81, 25)
        Me.BillingToolStripMenuItem.Text = "Billing"
        '
        'UnPaidBillsToolStripMenuItem
        '
        Me.UnPaidBillsToolStripMenuItem.Image = CType(resources.GetObject("UnPaidBillsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UnPaidBillsToolStripMenuItem.Name = "UnPaidBillsToolStripMenuItem"
        Me.UnPaidBillsToolStripMenuItem.Size = New System.Drawing.Size(154, 26)
        Me.UnPaidBillsToolStripMenuItem.Text = "Credit Bills"
        '
        'PaidBillToolStripMenuItem
        '
        Me.PaidBillToolStripMenuItem.Image = CType(resources.GetObject("PaidBillToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PaidBillToolStripMenuItem.Name = "PaidBillToolStripMenuItem"
        Me.PaidBillToolStripMenuItem.Size = New System.Drawing.Size(154, 26)
        Me.PaidBillToolStripMenuItem.Text = "Paid Bill"
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DailySalesToolStripMenuItem, Me.DailyTransactionToolStripMenuItem})
        Me.ReportsToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.ReportsToolStripMenuItem.Image = CType(resources.GetObject("ReportsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(92, 25)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'DailySalesToolStripMenuItem
        '
        Me.DailySalesToolStripMenuItem.Image = CType(resources.GetObject("DailySalesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DailySalesToolStripMenuItem.Name = "DailySalesToolStripMenuItem"
        Me.DailySalesToolStripMenuItem.Size = New System.Drawing.Size(211, 26)
        Me.DailySalesToolStripMenuItem.Text = "Daily Sales"
        '
        'DailyTransactionToolStripMenuItem
        '
        Me.DailyTransactionToolStripMenuItem.Image = CType(resources.GetObject("DailyTransactionToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DailyTransactionToolStripMenuItem.Name = "DailyTransactionToolStripMenuItem"
        Me.DailyTransactionToolStripMenuItem.Size = New System.Drawing.Size(211, 26)
        Me.DailyTransactionToolStripMenuItem.Text = "Transaction Details"
        '
        'MasterToolStripMenuItem
        '
        Me.MasterToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CategoryToolStripMenuItem, Me.ProductsToolStripMenuItem, Me.UsersToolStripMenuItem, Me.WaiterToolStripMenuItem})
        Me.MasterToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.MasterToolStripMenuItem.Image = CType(resources.GetObject("MasterToolStripMenuItem.Image"), System.Drawing.Image)
        Me.MasterToolStripMenuItem.Name = "MasterToolStripMenuItem"
        Me.MasterToolStripMenuItem.Size = New System.Drawing.Size(86, 25)
        Me.MasterToolStripMenuItem.Text = "Master"
        '
        'CategoryToolStripMenuItem
        '
        Me.CategoryToolStripMenuItem.Image = CType(resources.GetObject("CategoryToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CategoryToolStripMenuItem.Name = "CategoryToolStripMenuItem"
        Me.CategoryToolStripMenuItem.Size = New System.Drawing.Size(143, 26)
        Me.CategoryToolStripMenuItem.Text = "Category"
        '
        'ProductsToolStripMenuItem
        '
        Me.ProductsToolStripMenuItem.Image = CType(resources.GetObject("ProductsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ProductsToolStripMenuItem.Name = "ProductsToolStripMenuItem"
        Me.ProductsToolStripMenuItem.Size = New System.Drawing.Size(143, 26)
        Me.ProductsToolStripMenuItem.Text = "Products"
        '
        'UsersToolStripMenuItem
        '
        Me.UsersToolStripMenuItem.Image = CType(resources.GetObject("UsersToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UsersToolStripMenuItem.Name = "UsersToolStripMenuItem"
        Me.UsersToolStripMenuItem.Size = New System.Drawing.Size(143, 26)
        Me.UsersToolStripMenuItem.Text = "Users"
        '
        'WaiterToolStripMenuItem
        '
        Me.WaiterToolStripMenuItem.Image = CType(resources.GetObject("WaiterToolStripMenuItem.Image"), System.Drawing.Image)
        Me.WaiterToolStripMenuItem.Name = "WaiterToolStripMenuItem"
        Me.WaiterToolStripMenuItem.Size = New System.Drawing.Size(143, 26)
        Me.WaiterToolStripMenuItem.Text = "Waiter"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SoftwareToolStripMenuItem, Me.ComanyToolStripMenuItem})
        Me.AboutToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.AboutToolStripMenuItem.Image = CType(resources.GetObject("AboutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(80, 25)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalculatorToolStripMenuItem, Me.WordPadToolStripMenuItem, Me.NotePadToolStripMenuItem, Me.TaskManagerToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.ToolsToolStripMenuItem.Image = CType(resources.GetObject("ToolsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(69, 25)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'CalculatorToolStripMenuItem
        '
        Me.CalculatorToolStripMenuItem.Image = CType(resources.GetObject("CalculatorToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CalculatorToolStripMenuItem.Name = "CalculatorToolStripMenuItem"
        Me.CalculatorToolStripMenuItem.Size = New System.Drawing.Size(160, 24)
        Me.CalculatorToolStripMenuItem.Text = "Calculator"
        '
        'WordPadToolStripMenuItem
        '
        Me.WordPadToolStripMenuItem.Image = CType(resources.GetObject("WordPadToolStripMenuItem.Image"), System.Drawing.Image)
        Me.WordPadToolStripMenuItem.Name = "WordPadToolStripMenuItem"
        Me.WordPadToolStripMenuItem.Size = New System.Drawing.Size(160, 24)
        Me.WordPadToolStripMenuItem.Text = "WordPad"
        '
        'NotePadToolStripMenuItem
        '
        Me.NotePadToolStripMenuItem.Image = CType(resources.GetObject("NotePadToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NotePadToolStripMenuItem.Name = "NotePadToolStripMenuItem"
        Me.NotePadToolStripMenuItem.Size = New System.Drawing.Size(160, 24)
        Me.NotePadToolStripMenuItem.Text = "NotePad"
        '
        'TaskManagerToolStripMenuItem
        '
        Me.TaskManagerToolStripMenuItem.Image = CType(resources.GetObject("TaskManagerToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TaskManagerToolStripMenuItem.Name = "TaskManagerToolStripMenuItem"
        Me.TaskManagerToolStripMenuItem.Size = New System.Drawing.Size(160, 24)
        Me.TaskManagerToolStripMenuItem.Text = "TaskManager"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.ExitToolStripMenuItem.Image = CType(resources.GetObject("ExitToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(62, 25)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2, Me.ToolStripStatusLabel3})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 585)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1237, 22)
        Me.StatusStrip1.TabIndex = 7
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Image = CType(resources.GetObject("ToolStripStatusLabel1.Image"), System.Drawing.Image)
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(137, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Image = CType(resources.GetObject("ToolStripStatusLabel2.Image"), System.Drawing.Image)
        Me.ToolStripStatusLabel2.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(137, 17)
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.ToolTipText = "User"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Margin = New System.Windows.Forms.Padding(20, 3, 0, 2)
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(555, 17)
        Me.ToolStripStatusLabel3.Text = "Developed By Dbros Technology Pvt. Ltd.  Contact: 9843408105,9860486008. www.dbro" & _
    "stechnology.com"
        '
        'SoftwareToolStripMenuItem
        '
        Me.SoftwareToolStripMenuItem.Name = "SoftwareToolStripMenuItem"
        Me.SoftwareToolStripMenuItem.Size = New System.Drawing.Size(152, 26)
        Me.SoftwareToolStripMenuItem.Text = "Software"
        '
        'ComanyToolStripMenuItem
        '
        Me.ComanyToolStripMenuItem.Name = "ComanyToolStripMenuItem"
        Me.ComanyToolStripMenuItem.Size = New System.Drawing.Size(152, 26)
        Me.ComanyToolStripMenuItem.Text = "Company"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1237, 607)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MainForm"
        Me.Text = "DB Resturant Billing System"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents BillingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DailyTransactionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DailySalesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MasterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CategoryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsersToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WaiterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnPaidBillsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaidBillToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculatorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WordPadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NotePadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TaskManagerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents SoftwareToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComanyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
