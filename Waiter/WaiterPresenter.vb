﻿Public Class WaiterPresenter

    Dim _waiter As Waiter
    Dim _v As WaiterForm

    Sub New(ByVal view As Form)
        _v = view
    End Sub

    Sub New(ByVal view As Form, ByVal model As Waiter)
        _v = view
        _waiter = model
    End Sub

    'init while loading form
    Public Sub LoadForm()
        EnableBtn(False)
        LoadData()
    End Sub

    Sub Reset()
        _v.txtName.Clear()
        _v.txtPhone.Clear()
        _v.txtEmail.Clear()
        _v.txtAddress.Clear()
    End Sub
    'control buttons
    Public Sub EnableBtn(ByVal bool As Boolean)
        _v.btnDelete.Enabled = bool
        _v.btnUpdate.Enabled = bool
        _v.btnNew.Enabled = bool
    End Sub

    'control save button
    Public Sub EnableSave(ByVal bool As Boolean)
        _v.btnSave.Enabled = bool
    End Sub

    'create new waiter
    Sub Save()
        If _waiter.Create() Then
            LoadData()
        Else
            MessageBox.Show("Process Failed", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    'load waiter in datagridview
    Sub LoadData()
        _v.RadioButton1.Checked = True
        _waiter = New Waiter()
        _v.DataGridView1.DataSource = _waiter.Read()
    End Sub

    Public Sub Edit()
        Try
            Dim dr As DataGridViewRow = _v.DataGridView1.SelectedRows(0)
            _v.txtName.Text = dr.Cells("WaiterName").Value.ToString
            _v.txtPhone.Text = dr.Cells("Phone").Value.ToString

            _v.txtEmail.Text = dr.Cells("Email").Value.ToString
            _v.txtAddress.Text = dr.Cells("Address").Value.ToString

            Dim status As String = Convert.ToInt32(dr.Cells("Status").Value)

            If status = 1 Then
                _v.RadioButton1.Checked = True
            Else
                _v.RadioButton2.Checked = True
            End If

            EnableBtn(True)
            EnableSave(False)

        Catch ex As Exception
            Console.WriteLine("Error: Edit product" & ex.ToString)
        End Try

    End Sub

    Public Sub Update()
        ValidateFields()
        Try
            Dim dr As DataGridViewRow = _v.DataGridView1.SelectedRows(0)
            _waiter.WaiterId = dr.Cells("WaiterId").Value.ToString
            If _waiter.Update() Then
                Me.LoadData()
                Reset()
                MessageBox.Show("Successfully Recorded", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Process Failed", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            Console.WriteLine("Error: update product" & ex.ToString)
        End Try
        
    End Sub

    Public Sub ValidateFields()
        If _v.txtName Is Nothing Or _v.txtName.Text = "" Then
            MessageBox.Show("Enter Name", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            _v.txtName.Focus()
            Return
        End If
    End Sub

    Sub Delete()
        If MessageBox.Show("Do you Want to Delete Permanently. You cannot recover it", "Alert! Are you sure! ", MessageBoxButtons.OKCancel) = DialogResult.OK Then
            Dim dr As DataGridViewRow = _v.DataGridView1.SelectedRows(0)
            _waiter.WaiterId = dr.Cells("WaiterId").Value.ToString
            If _waiter.Delete() Then
                Me.LoadData()
                MessageBox.Show("Successfully Recorded", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Process Failed", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
        
    End Sub
End Class
