﻿Public Class WaiterForm
    Implements IWaiter
    Public Property _w As Waiter = New Waiter()

    Public Property waiter As Waiter Implements IWaiter.waiter
        Get
            _w.WaiterName = txtName.Text.Trim()
            _w.Address = txtAddress.Text.Trim()
            _w.Email = txtEmail.Text.Trim()
            _w.Phone = txtPhone.Text.Trim()

            If RadioButton1.Checked Then
                _w.Status = 1
            Else
                _w.Status = 0
            End If

            Return _w

        End Get
        Set(value As Waiter)
            txtName.Text = value.WaiterName
            txtAddress.Text = value.Address
            txtEmail.Text = value.Email
            txtPhone.Text = value.Phone
        End Set
    End Property

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim p As WaiterPresenter = New WaiterPresenter(Me, waiter)
        p.Save()
    End Sub

    Private Sub WaiterForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim p As WaiterPresenter = New WaiterPresenter(Me)
        p.LoadForm()
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim p As WaiterPresenter = New WaiterPresenter(Me, waiter)
        p.Update()
    End Sub

    Private Sub DataGridView1_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.RowHeaderMouseClick
        Dim p As WaiterPresenter = New WaiterPresenter(Me, waiter)
        p.Edit()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim p As WaiterPresenter = New WaiterPresenter(Me, waiter)
        p.Delete()
    End Sub
End Class