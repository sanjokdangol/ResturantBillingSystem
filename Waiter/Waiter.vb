﻿Imports System.Data.SqlClient

''' <summary>
''' Class Waiter.
''' </summary>
Public Class Waiter
    Property WaiterId As Integer
    Property WaiterName As String
    Property Phone As String
    Property Address As String
    Property Email As String
    Property Status As Integer


    Dim _table As String = "Waiter"

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString



    Public Function Create() As Boolean
        Dim result As Boolean
        q = "insert into " & _table & " (WaiterName, Phone, Address, Email, Status) values(@name, @phone, @address, @email, @status)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", Me.WaiterName)
            cmd.Parameters.AddWithValue("@phone", Me.Phone)
            cmd.Parameters.AddWithValue("@address", Me.Address)
            cmd.Parameters.AddWithValue("@email", Me.Email)
            cmd.Parameters.AddWithValue("@status", Me.Status)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If
            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Waiter" & ex.ToString)
        End Try
        Return result
    End Function

    Public Function Read() As DataTable

        q = "SELECT * FROM " & _table & " where status=1 order by WaiterId Desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    Public Function Update() As Boolean
        Dim result As Boolean = False
        q = "UPDATE " & _table & " SET WaiterName=@name, Phone=@phone, Address=@address, Email=@email, Status=@status WHERE WaiterId = @id"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", Me.WaiterName)
            cmd.Parameters.AddWithValue("@phone", Me.Phone)
            cmd.Parameters.AddWithValue("@address", Me.Address)
            cmd.Parameters.AddWithValue("@email", Me.Email)
            cmd.Parameters.AddWithValue("@status", Me.Status)
            cmd.Parameters.AddWithValue("@id", Me.WaiterId)
            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Public Function Delete() As Boolean
        Dim result As Boolean = False
        q = "DELETE FROM " & _table & " WHERE WaiterId = @ID"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@ID", Me.WaiterId)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function


    Shared Function GetWaiterIdByName(ByVal WaiterName As String) As Integer
        Dim con As SqlConnection
        Dim cmd As SqlCommand
        Dim q As String

        Dim res As Integer = 1
        q = "select WaiterId from Waiter Where WaiterName=@WaiterName"
        Try
            con = New SqlConnection(ConnectionString.GetConnectionString())
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("WaiterName", WaiterName)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = reader("WaiterId")
                Else
                    MessageBox.Show("CouldNot find waiter Id")
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsBillNoExist" & ex.ToString)
        End Try
        Return res
    End Function

    Shared Function GetWaiterNameById(ByVal Id As String) As String
        Dim con As SqlConnection
        Dim cmd As SqlCommand
        Dim q As String

        Dim res As String = ""
        q = "select WaiterName from Waiter Where WaiterId=@id"
        Try
            con = New SqlConnection(ConnectionString.GetConnectionString())
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@id", Id)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()

                res = reader("WaiterName")
                Exit While
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error " & ex.ToString)
        End Try
        Return res
    End Function
End Class
