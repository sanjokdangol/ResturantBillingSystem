﻿Imports System.Data.SqlClient

Public Class DBFun

   
    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString

    Public Function GetDataTableByQuery(ByVal q As String, ByVal _table As String) As DataTable
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at dbfun " & ex.ToString)
        End Try

        Return dt
    End Function

End Class
