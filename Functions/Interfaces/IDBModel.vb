﻿Public Interface IDBModel
    Sub Create()
    Sub Read()
    Sub Update()
    Sub Delete()

End Interface
