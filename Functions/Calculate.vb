﻿Public Class Calculate
    Public Shared Function CalculateByIndex(ByVal gv As DataGridView, ByVal index As Integer) As Double
        Dim sum As Double = 0.0
        Dim j As Integer = 0
        Dim i As Integer = 0
        Try
            j = gv.RowCount
            While i < j
                sum += Convert.ToDouble(gv(index, i).Value)
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return sum
    End Function

    Public Shared Function CalculateNagetiveNumberByIndex(ByVal gv As DataGridView, ByVal index As Integer) As Double
        Dim sum As Double = 0.0
        Dim j As Integer = 0
        Dim i As Integer = 0
        Dim n As Double = 0.0
        Try
            j = gv.RowCount
            While i < j
                n = gv(index, i).Value
                If n < 0 Then
                    sum += Convert.ToDouble(gv(index, i).Value)
                End If
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return sum
    End Function

    Public Shared Function CalculatePositiveNumberByIndex(ByVal gv As DataGridView, ByVal index As Integer) As Double
        Dim sum As Double = 0.0
        Dim j As Integer = 0
        Dim i As Integer = 0
        Dim n As Double = 0.0
        Try
            j = gv.RowCount
            While i < j
                n = gv(index, i).Value
                If n > 0 Then
                    sum += Convert.ToDouble(gv(index, i).Value)
                End If
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return sum
    End Function

    Public Shared Function CalculateFromList(ByVal listView As ListView, ByVal col As Integer) As Integer

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Double = 0

        Try
            j = listView.Items.Count

            While i < j
                k = k + Convert.ToInt32(listView.Items(i).SubItems(col).Text)
                i += 1
            End While

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

        Return k

    End Function
End Class
