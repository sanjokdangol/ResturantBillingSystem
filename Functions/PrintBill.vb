﻿Imports System.Drawing.Printing

Public Class PrintBill

    Dim _v As PayBillForm
    Dim _m As Transation

    Sub New(ByVal view As Form, ByVal model As Transation)
        _v = view
        _m = model
    End Sub

    '// The sample data.
    Private Headers As String() = {"Sn", "Particulars", "Rate", "Qty", "Total"}
    'bill details from listview
    Dim data(,) As String
    'no of item from listview
    Dim count As Integer = 0
    '// Print the document's page.
    '// Note that this version doesn't handle multiple pages.
    Public Sub PrintPage(ByVal sender As Object, ByVal e As Printing.PrintPageEventArgs, ByVal Data As String(,), ByVal count As Integer)
        'required data
        Me.data = Data 'bill details from listview
        Me.count = count    'no of item from listview


        Dim leftMargin As Single = e.MarginBounds.Left

        Dim topMargin As Single = e.MarginBounds.Top

    

        Dim footer As Integer = 200
        '// Use this font.
        Dim Font_Bold As Font = New Font("Courier New", 9, FontStyle.Bold)
        Dim Font_Normal As Font = New Font("Courier New", 9)

        Dim header_font As Font = Font_Bold

        Dim body_font As Font = Font_Normal

        '// We'll skip this much space between rows.
        Dim line_spacing As Integer = 20

        '//for string format rec box
        '//used in footer of bill to make right alignment
        Dim rect_width As Integer = 220
        Dim rect_height As Integer = 30

        '// See how wide the columns must be.
        Dim column_widths As Integer() = FindColumnWidths(e.Graphics, header_font, body_font)

        ' // Start at the left margin.
        Dim x As Integer = 5

        Dim startX As Integer = 10
        Dim startY As Integer = 5
        Dim Offset As Integer = 0

        Dim rectTop As Rectangle = New Rectangle(startX, startY + Offset, 230, 40)

        Dim stringFormat As StringFormat = New StringFormat()
        stringFormat.Alignment = StringAlignment.Center
        stringFormat.LineAlignment = StringAlignment.Center

        e.Graphics.DrawString("RATOBHALE FASTFOOD", New Font("Courier New", 10),
                    New SolidBrush(Color.Black), rectTop, stringFormat)

        Offset = Offset + 15

        Dim rectTop1 As Rectangle = New Rectangle(startX, startY + Offset, 230, 40)

        '// & TONDOORI RESTURENT
        e.Graphics.DrawString("& TONDOORI RESTURANT", New Font("Courier New", 10),
                    New SolidBrush(Color.Black), rectTop1, stringFormat)

        Offset = Offset + 15
        Dim rectTop2 As Rectangle = New Rectangle(startX, startY + Offset, 230, 40)

        e.Graphics.DrawString("KATHMANDU, NEPAL", New Font("Courier New", 9),
                            New SolidBrush(Color.Black), rectTop2, stringFormat)

        Offset = Offset + 15
        Dim rectTop3 As Rectangle = New Rectangle(startX, startY + Offset, 230, 40)

        e.Graphics.DrawString(" INVOICE", New Font("Courier New", 9),
                            New SolidBrush(Color.Black), rectTop3, stringFormat)

        '//014381136
        '//Pan no.600945970
        Offset = Offset + 30
        e.Graphics.DrawString("PAN No.: 600945970", New Font("Courier New", 9),
                            New SolidBrush(Color.Black), startX, startY + Offset)

        Offset = Offset + 15
        e.Graphics.DrawString("Contact: 01-4381136", New Font("Courier New", 9),
                            New SolidBrush(Color.Black), startX, startY + Offset)

        Offset = Offset + 15
        e.Graphics.DrawString("BILL No: " + _m.BillNo, New Font("Courier New", 9),
                 New SolidBrush(Color.Black), startX, startY + Offset)

        Offset = Offset + 15
        e.Graphics.DrawString("Date : " + _m.BillDate,
                 New Font("Courier New", 9),
                 New SolidBrush(Color.Black), startX, startY + Offset)
        Offset = Offset + 15

        '// Print by columns.



            Dim col As Integer = 0
            Try
            While col < Headers.Length
                '// Print the header.
                Dim y As Integer = Offset + startY
                If col = 0 Then
                    Dim u1 As String = "----------------------------------"
                    e.Graphics.DrawString(u1, New Font("Courier New", 9),
                     New SolidBrush(Color.Black), x, y + 15)

                End If
                y += line_spacing * 1.5

                e.Graphics.DrawString(Headers(col), header_font, Brushes.Blue, x, y)

                y += 15
                If (col = 0) Then
                    Dim u1 As String = "----------------------------------"
                    e.Graphics.DrawString(u1, New Font("Courier New", 9), New SolidBrush(Color.Black), x, y)
                End If
                y += 15
                '// Print the items in the column.         

                Dim row As Integer = 0
                While row < count
                    e.Graphics.DrawString(Data(row, col), body_font, Brushes.Black, x, y)
                    y += line_spacing
                    footer = y
                    row = row + 1

                End While

                'Move to the next column.
                x += column_widths(col) + 5
                col = col + 1

            End While

            Catch ex As Exception
                Console.WriteLine(ex)
        End Try
        
        
        Dim underLine As String = "----------------------------------"

        Dim format As StringFormat = New StringFormat(StringFormatFlags.DirectionRightToLeft)

        e.Graphics.DrawString(underLine, New Font("Courier New", 9),
         New SolidBrush(Color.Black), 10, footer)
        footer += line_spacing

        Dim rect As RectangleF = New RectangleF(10.0F, footer, rect_width, rect_height)

        e.Graphics.DrawString("SubTotal : Rs." + _m.SubTotal, New Font("Courier New", 9),
                    New SolidBrush(Color.Black), rect, format)

        footer += line_spacing
        Dim rect1 As RectangleF = New RectangleF(10.0F, footer, rect_width, rect_height)

        e.Graphics.DrawString("Discount " + _m.DiscountPer + "% : Rs." + _m.DiscountAmount, New Font("Courier New", 9),
         New SolidBrush(Color.Black), rect1, format)

        footer += line_spacing
        Dim rect2 As RectangleF = New RectangleF(10.0F, footer, rect_width, rect_height)

        e.Graphics.DrawString("Tax " + _m.VATPer + "% : Rs." + _m.VATAmount, New Font("Courier New", 9),
        New SolidBrush(Color.Black), rect2, format)

        footer += line_spacing
        Dim rect3 As RectangleF = New RectangleF(10.0F, footer, rect_width, rect_height)

        e.Graphics.DrawString("Total : Rs." + _m.GrandTotal, New Font("Courier New", 9),
       New SolidBrush(Color.Black), rect3, format)

        footer += line_spacing
        Dim rect4 As RectangleF = New RectangleF(10.0F, footer, rect_width, rect_height)

        e.Graphics.DrawString("Thank you! Visit Again", New Font("Courier New", 9),
         New SolidBrush(Color.Black), rect4, format)



        '//DrawGrid(e, y)
        'e.HasMorePages = False
    End Sub

    Private Sub FillGap(ByVal h As Integer)
        h += 15
    End Sub
    '// Figure out how wide each column should be.
    Public Function FindColumnWidths(ByVal gr As Graphics, ByVal header_font As Font, ByVal body_font As Font) As Integer()
        '// Make room for the widths.
        Dim widths(Me.Headers.Length) As Integer
        Try
            '// Find the width for each column.
            Dim col As Integer = 0
            While col < widths.Length - 1

                '// Check the column header.
                widths(col) = gr.MeasureString(Me.Headers(col), header_font).Width

                '//Console.Write(values.GetUpperBound(0));
                Dim row As Integer = 0
                While row < count

                    Dim value_width As Integer = gr.MeasureString(Me.data(row, col), body_font).Width
                    If widths(col) < value_width Then
                        widths(col) = value_width
                    End If
                    row = row + 1
                End While

                '// Add some extra space.
                'widths(col) += 5
                col = col + 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return widths

    End Function
End Class
