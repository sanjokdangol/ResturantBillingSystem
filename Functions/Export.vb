﻿Imports Excel = Microsoft.Office.Interop.Excel
Public Class Export
    Public Sub ExportToExcel(ByVal dataGridView1 As DataGridView)
        Dim rowsTotal As Integer = 0
        Dim colsTotal As Integer = 0
        Dim I As Integer = 0
        Dim j As Integer = 0
        Dim iC As Integer = 0
        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
        Dim xlApp As Excel.Application = New Excel.Application()

        Try
            Dim excelBook As Excel.Workbook = xlApp.Workbooks.Add()
            Dim excelWorksheet As Excel.Worksheet = CType(excelBook.Worksheets(1), Excel.Worksheet)
            xlApp.Visible = True
            rowsTotal = dataGridView1.RowCount - 1
            colsTotal = dataGridView1.Columns.Count - 1
            Dim _with1 = excelWorksheet
            _with1.Cells.[Select]()
            _with1.Cells.Delete()

            For iC = 0 To colsTotal
                _with1.Cells(1, iC + 1).Value = dataGridView1.Columns(iC).HeaderText
            Next

            For I = 0 To rowsTotal - 1

                For j = 0 To colsTotal
                    _with1.Cells(I + 2, j + 1).value = dataGridView1.Rows(I).Cells(j).Value
                Next
            Next

            _with1.Rows("1:1").Font.FontStyle = "Bold"
            _with1.Rows("1:1").Font.Size = 12
            _with1.Cells.Columns.AutoFit()
            _with1.Cells.[Select]()
            _with1.Cells.EntireColumn.AutoFit()
            _with1.Cells(1, 1).[Select]()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
        Finally
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.[Default]
            xlApp = Nothing
        End Try
    End Sub

End Class
