﻿Public Class Calculator
    Inherits Transation

    Public Function CalculateDiscount(ByVal subTotal As String, ByVal discountPercent As String) As Double

        Dim discount As Double
        Try
            discount = Convert.ToDouble((Convert.ToDouble(subTotal) * (Convert.ToDouble(discountPercent) / 100.0)))

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return Math.Round(discount, 2)
    End Function

    Public Function CalculateTax(ByVal subTotal As String, ByVal taxPercent As String, ByVal discount As String) As Double
        Dim tax As Double
        Try
            tax = Convert.ToDouble((Convert.ToDouble(subTotal) - Convert.ToDouble(discount)) * Convert.ToDouble(taxPercent) / 100)

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return Math.Round(tax, 2)

    End Function

    Public Function CalculateTotal(ByVal subTotal As String, ByVal taxAmount As String, ByVal discountAmount As String) As Double
        Dim total As Double
        Try
            total = Convert.ToDouble(subTotal) + Convert.ToDouble(taxAmount) + Convert.ToDouble(ServiceCharge) - Convert.ToDouble(discountAmount)
            total = Math.Round(total, 2)
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return Math.Round(total, 2)
    End Function

    Public Function CalculateChange(ByVal total As String, ByVal cash As String) As Double
        Dim change As Double
        Try
            change = Convert.ToDouble(cash) - Convert.ToDouble(total)
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return Math.Round(change, 2)
    End Function

    Public Function CalculateSubTotal(ByVal listView As ListView) As Double

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Double = 0

        Try
            j = listView.Items.Count

            For i = 0 To i < j - 1
                k = k + Convert.ToDouble(listView.Items(i).SubItems(5).Text)
                k = Math.Round(k, 2)
            Next i

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

        Return k

    End Function

    Public Function CalculateFromGridByIndex(ByVal gv As DataGridView, ByVal index As Integer) As Double
        Dim sum As Double = 0.0
        Dim j As Integer = 0
        Dim i As Integer = 0
        Try
            j = gv.RowCount
            While i < j
                sum += Convert.ToDouble(gv(index, i).Value)
                i += 1
            End While
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return sum
    End Function



End Class
