﻿Public Class Search

    Sub SearchInGridView(ByVal DataGridView1 As DataGridView, ByVal col As String, ByVal searchText As String)
        Try
            DataGridView1.DataSource.DefaultView.RowFilter = String.Format(" " + col + " like '" + searchText + "%'")

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub

    

    Shared Sub SearchOnComboBox(ByVal cboCategory As ComboBox)
        Dim index As Integer
        Dim actual As String
        Dim found As String

        ' Store the actual text that has been typed.
        actual = cboCategory.Text

        ' Find the first match for the typed value.
        index = cboCategory.FindString(actual)

        ' Get the text of the first match.
        If (index > -1) Then
            found = cboCategory.Items(index).ToString()

            ' Select this item from the list.
            cboCategory.SelectedIndex = index

            ' Select the portion of the text that was automatically
            ' added so that additional typing will replace it.
            cboCategory.SelectionStart = actual.Length
            cboCategory.SelectionLength = found.Length
        End If
    End Sub

End Class
