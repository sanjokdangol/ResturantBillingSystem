﻿Imports System.Data.SqlClient
Public Class Customer

    Property CustomerId As Integer
    Property CustomerName As String
    Property ContactNo1 As String
    Property cAddress As String
    Property City As String
    Property Email As String
    Property Status As Integer
    Property ContactNo As String
    Property Notes As String

    Public Shared cName As String
    Public Shared cPhone As String


    Dim _table As String = "Customer"

    'DB variables
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Dim conString As String = ConnectionString.GetConnectionString



    Public Function Create() As Boolean
        Dim result As Boolean

        q = "insert into " & _table & " (CustomerName, ContactNo,ContactNo1, Address, City, Email, Status, Notes) values(@name, @ContactNo,@ContactNo1, @cAddress, @city, @email, @status, @Notes)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", Me.CustomerName)
            cmd.Parameters.AddWithValue("@ContactNo", Me.ContactNo)
            cmd.Parameters.AddWithValue("@ContactNo1", Me.ContactNo1)
            cmd.Parameters.AddWithValue("@Notes", Me.Notes)
            cmd.Parameters.AddWithValue("@cAddress", Me.cAddress)
            cmd.Parameters.AddWithValue("@email", Me.Email)
            cmd.Parameters.AddWithValue("@status", 1)
            cmd.Parameters.AddWithValue("@city", City)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                CustomerId = GetMaxId()
                result = True
            Else
                result = False
            End If
            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Waiter" & ex.ToString)
        End Try
        Return result
    End Function

    Public Function Read() As DataTable

        q = "SELECT * FROM " & _table & " order by CustomerId Desc"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    Public Function Update() As Boolean
        Dim result As Boolean = False
        q = "UPDATE " & _table & " SET CustomerName=@name, ContactNo=@ContactNo, ContactNo1=@ContactNo1, Address=@cAddress, Email=@email, Status=@status, Notes=@Notes WHERE CustomerId = @id"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", Me.CustomerName)
            cmd.Parameters.AddWithValue("@ContactNo", Me.ContactNo)
            cmd.Parameters.AddWithValue("@ContactNo1", Me.ContactNo1)
            cmd.Parameters.AddWithValue("@Notes", Me.Notes)
            cmd.Parameters.AddWithValue("@cAddress", Me.cAddress)
            cmd.Parameters.AddWithValue("@email", Me.Email)
            cmd.Parameters.AddWithValue("@status", Me.Status)
            cmd.Parameters.AddWithValue("@id", Me.CustomerId)
            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Public Function Delete() As Boolean
        Dim result As Boolean = False
        q = "DELETE FROM " & _table & " WHERE CustomerId = @ID"

        Try
            conString = ConnectionString.GetConnectionString
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@ID", Me.CustomerId)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        Return result
    End Function

    Function GetMaxId() As Integer
        Dim con As SqlConnection
        Dim cmd As SqlCommand
        Dim q As String

        Dim res As Integer
        q = "select Max(CustomerId) from " & _table
        Try
            con = New SqlConnection(ConnectionString.GetConnectionString())
            con.Open()
            cmd = New SqlCommand(q, con)

            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = reader(0)
                Else
                    MessageBox.Show("CouldNot find Max Id")
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsBillNoExist" & ex.ToString)
        End Try
        Return res
    End Function

    Public Function IsCustomerExist() As Boolean
        Dim res As Boolean = False
        q = "select count(CustomerName) from " & _table & " Where CustomerName=@CustomerName"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("CustomerName", CustomerName)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsCustomerExist" & ex.ToString)
        End Try
        Return res
    End Function

    Public Function IsContactNoExist() As Boolean
        Dim res As Boolean = False
        q = "select count(ContactNo) from " & _table & " Where ContactNo=@ContactNo"
        Try
            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("ContactNo", ContactNo)
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            While reader.Read()
                If Integer.Parse(reader(0)) > 0 Then
                    res = True
                Else
                    res = False
                End If
            End While
            con.Close()
        Catch ex As Exception
            Console.WriteLine("Error at IsCustomerExist" & ex.ToString)
        End Try
        Return res
    End Function

    Public Function ReadCustomerList() As DataTable

        q = "SELECT CustomerId, CustomerName, ContactNo,ContactNo1, Address, Email FROM " & _table & " order by CustomerName"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    Public Function ReadCustomerByName() As DataTable

        q = "SELECT CustomerId, CustomerName, ContactNo,ContactNo1, Address, Email FROM " & _table & " where CustomerName=@CustomerName order by CustomerName"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("CustomerName", CustomerName)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    Public Function ReadCustomerByContactNo() As DataTable

        q = "SELECT CustomerId, CustomerName, ContactNo,ContactNo1, Address, Email FROM " & _table & " where ContactNo=@ContactNo order by CustomerName"

        Try

            con = New SqlConnection(conString)
            con.Open()
            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("ContactNo", ContactNo)
            adapter = New SqlDataAdapter(cmd)
            sBuilder = New SqlCommandBuilder(adapter)
            ds = New DataSet()
            adapter.Fill(ds, _table)
            dt = ds.Tables(_table)
            con.Close()

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try

        Return dt

    End Function

    

End Class
