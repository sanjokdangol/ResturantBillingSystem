﻿Public Class CustomerForm
    Implements ICustomer

    Dim _c As Customer = New Customer()
    ''' <summary>
    ''' Gets or sets the flag.
    ''' flag = 1 refers to credit sales
    ''' and flag =2 refers to cash sales
    ''' </summary>
    ''' <value>The flag.</value>
    Property flag As Integer = 0

    Public Property customer As Customer Implements ICustomer.customer
        Get
            _c.CustomerName = txtName.Text.Trim()
            _c.cAddress = txtAddress.Text.Trim()
            _c.Email = txtEmail.Text.Trim()
            _c.ContactNo = txtContact.Text.Trim()
            _c.ContactNo1 = txtLandline.Text.Trim()
            _c.Notes = txtNotes.Text
            _c.City = txtCity.Text.Trim()
            Return _c
        End Get
        Set(value As Customer)

        End Set
    End Property

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim p As CustomerPresenter = New CustomerPresenter(Me, customer)
        p.Save()
    End Sub

    Private Sub txtName_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged
        
    End Sub

    Private Sub txtName_Leave(sender As Object, e As EventArgs) Handles txtName.Leave
        Try
            Dim c As Customer = New Customer()
            'donot remove this is required to check user exist
            c.CustomerName = txtName.Text
            If c.IsCustomerExist() Then
                MessageBox.Show("Customer Name Already Exist. Choose From List", "Alert", MessageBoxButtons.OK)
                Dim frm As CustomerList = New CustomerList(c)
                frm.ShowDialog()
                
                _c.CustomerId = frm.CustomerId
                If Not _c.CustomerId = Nothing Then
                    flag = 1 'indicate this is credit bill
                    Me.Close()
                End If


            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
       
    End Sub

    Private Sub txtContact_Leave(sender As Object, e As EventArgs) Handles txtContact.Leave
        Try
            Dim c As Customer = New Customer()
            'donot remove this is required to check user exist
            c.ContactNo = txtContact.Text
            If c.IsContactNoExist() Then
                MessageBox.Show("Contact No Already Exist. Choose From List. You cannot create same customer Again", "Alert", MessageBoxButtons.OK)
                Dim frm As CustomerList = New CustomerList(c)
                frm.ShowDialog()

                _c.CustomerId = frm.CustomerId
                If Not _c.CustomerId = Nothing Then
                    flag = 1 'indicate this is credit bill
                    Me.Close()
                End If


            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
    End Sub
End Class