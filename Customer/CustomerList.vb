﻿Public Class CustomerList
    Property CustomerId As String
    Property CustomerName As String
    Property _customer As Customer
    Sub New(ByVal c As Customer)

        ' This call is required by the designer.
        InitializeComponent()
        _customer = c
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub CustomerList_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not _customer.CustomerName = Nothing Then
            DataGridView1.DataSource = _customer.ReadCustomerByName()
        End If

        If Not _customer.ContactNo = Nothing Then
            DataGridView1.DataSource = _customer.ReadCustomerByContactNo()
        End If

    End Sub

    Private Sub DataGridView1_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.RowHeaderMouseClick
        If DataGridView1.SelectedRows.Count > 0 Then
            Me.CustomerId = DataGridView1.SelectedRows(0).Cells(0).Value.ToString()
            Me.CustomerName = DataGridView1.SelectedRows(0).Cells(1).Value.ToString()
            Me.Close()
        End If
    End Sub
End Class