﻿Public Class CustomerPresenter

    Dim _customer As Customer
    Dim _c As CustomerForm

    Sub New()

    End Sub

    Sub New(ByVal view As Form, ByVal model As Customer)
        _c = view
        _customer = model
    End Sub



    Sub Save()
        If _c.customer.CustomerName = Nothing Then
            MessageBox.Show("Customer Name is Required", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If
        If _c.customer.ContactNo = Nothing Then
            MessageBox.Show("Contact No is Required", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        If _c.customer.cAddress = Nothing Then
            MessageBox.Show("Address is Required", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If
        Customer.cName = _c.customer.CustomerName
        Customer.cPhone = _c.customer.ContactNo
        'old customer
        If _customer.IsContactNoExist() Then
            _c.Close()
            _c.flag = 1
        Else
            'new customer
            If _customer.Create() Then
                _c.Close()
                _c.flag = 1
            Else
                MessageBox.Show("Process Failed", "Alert", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        End If
        
    End Sub
End Class
