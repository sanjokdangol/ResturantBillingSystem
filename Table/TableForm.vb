﻿Public Class TableForm

    Public _m As Table = New Table()
    Public order As BillOrder = New BillOrder()
 
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim p As TablePresenter = New TablePresenter(Me)
        p.LoadOrders()
        TableLayoutPanel1.Controls.Clear()
        TableLayoutPanel1.RowStyles.Clear()
        p.LoadTables()
        
    End Sub

    Public Sub ClickHandler(ByVal sender As Object, ByVal e As  _
   System.EventArgs)
        Try

            Dim p As TablePresenter = New TablePresenter(Me)
            _m.TableName = CType(sender, Button).Name
            'BillOrder.TableNo = _m.TableName
            'txtTblNo.Text = _m.TableName

            If TablePresenter.IsBusy(_m.TableName, ListView1) Then

                Dim p1 As PayBillPresenter = New PayBillPresenter(Me)
                p1.PayBill()
                Return
            End If

            Dim frm As OrderForm = New OrderForm(Me)
            frm.TblNo = _m.TableName

            'send bill it to orderForm in model OrderBill
            frm.Show()
            
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        
    End Sub

    Private Sub ListView1_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles ListView1.ItemSelectionChanged
        'Dim p As OrderPresenter = New OrderPresenter(Me)
        'p.ReadDataFromSelection()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        Dim p As PayBillPresenter = New PayBillPresenter(Me)
        p.PayBill()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim p As TablePresenter = New TablePresenter(Me)
        p.UpdateBill()
    End Sub


    Private Sub ListView1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles ListView1.MouseDoubleClick
        Dim p As PayBillPresenter = New PayBillPresenter(Me)
        p.PayBill()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Dim frm As ConfirmationForm = New ConfirmationForm()
        frm.ShowDialog()
        If frm.Confirmation = 1 Then
            If ListView1.SelectedItems.Count > 0 Then
                Dim t As TablePresenter = New TablePresenter(Me)
                Bill.CancelBill(t.ReadBillIdFromSelection())
                t.LoadOrders()
                TableLayoutPanel1.Controls.Clear()
                TableLayoutPanel1.RowStyles.Clear()
                t.LoadTables()
            End If
        End If
        
    End Sub

    Private Sub FlowLayoutPanel1_Paint(sender As Object, e As PaintEventArgs) Handles FlowLayoutPanel1.Paint

    End Sub
End Class