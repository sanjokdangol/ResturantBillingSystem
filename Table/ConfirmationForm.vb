﻿Public Class ConfirmationForm

    Property Confirmation As Integer = 0

    Private Sub ConfirmationForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If txtUserName.Text = Nothing Or txtPassword.Text = Nothing Then
            MessageBox.Show("You Must Enter Username and Password", "Alert", MessageBoxButtons.OK)
            Return
        End If
        Try
            Dim u As User = New User()
            u.UserName = txtUserName.Text
            If u.IsUserAdmin() Then

                Dim userExist As Boolean = u.IsOldPasswordMatched(txtUserName.Text, txtPassword.Text)
                If userExist Then
                    Me.Confirmation = 1
                    Me.Close()
                Else
                    MessageBox.Show("InCorrect Username & Password", "Alert", MessageBoxButtons.OK)
                    Return
                End If
            Else
                MessageBox.Show("You Do not Have Privilege To Cancel This Order. Contact Your Admin", "No Authority", MessageBoxButtons.OK)
                Return
            End If
        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
       
    End Sub
End Class