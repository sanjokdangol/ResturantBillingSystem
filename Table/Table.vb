﻿Imports System.Data.SqlClient

Public Class Table
    Property TableName As String
    Property TblStatus As Integer


    Public Shared _table As String = "HotelTable"

    'DB variables
    Public Shared con As SqlConnection
    Public Shared cmd As SqlCommand
    Public Shared q As String
    Dim adapter As SqlDataAdapter
    Dim sBuilder As SqlCommandBuilder
    Dim dt = New DataTable()
    Dim ds As New DataSet
    Dim changes As DataSet
    Public Shared conString As String = ConnectionString.GetConnectionString

    Public Shared Function LockTable(ByVal name As String) As Boolean
        Return SetTableStatus(name, 1)
    End Function

    Public Shared Function ReleaseTable(ByVal name As String) As Boolean
        Return SetTableStatus(name, 0)
    End Function

    Public Shared Function SetTableStatus(ByVal name As String, ByVal status As Integer) As Boolean
        Dim result = False
        q = "insert into " & _table & " (TableName, Status) values(@name, @status)"

        Try
            con = New SqlConnection(conString)
            con.Open()

            cmd = New SqlCommand(q, con)
            cmd.Parameters.AddWithValue("@name", name)
            cmd.Parameters.AddWithValue("@status", 1)

            Dim row As Integer = cmd.ExecuteNonQuery()

            If row > 0 Then
                result = True
            Else
                result = False
            End If

            con.Close()

        Catch ex As Exception
            Console.WriteLine("Error at Create Order" & ex.ToString)
        End Try

        Return result
    End Function

End Class
