﻿Imports System.Data.SqlClient
Public Class TablePresenter
    Dim _v As TableForm
    Dim _m As Table
 

    Sub New()

    End Sub

    Sub New(ByVal view As Form)
        _v = view
    End Sub

    Sub New(ByVal view As Form, model As Table)
        _v = view
        _m = model
    End Sub

    Dim NRow As Integer = 5
    Dim NCol As Integer = 4
    Dim BtnArray((NRow + 1) * (NCol + 1) - 1) As Button


    Sub LoadTables()

        _v.TableLayoutPanel1.Controls.Clear()
        _v.TableLayoutPanel1.RowStyles.Clear()

        _v.TableLayoutPanel1.Size = _v.ClientSize
        For i As Integer = 0 To BtnArray.Length - 1
            BtnArray(i) = New Button()
            BtnArray(i).Anchor = AnchorStyles.Top Or AnchorStyles.Bottom Or AnchorStyles.Left Or AnchorStyles.Right

            BtnArray(i).Name = CStr(i)
            BtnArray(i).Height = 100
            BtnArray(i).Width = 250
            If IsTableBusy(i) Then
                BtnArray(i).BackColor = Color.Crimson
                BtnArray(i).ForeColor = Color.White

            Else
                BtnArray(i).Text = CStr(i)
            End If
            _v.TableLayoutPanel1.Controls.Add(BtnArray(i), i Mod (NCol + 1), i \ (NCol + 1))
            AddHandler BtnArray(i).Click, AddressOf _v.ClickHandler
        Next

    End Sub
   
    'load orders in listview
    Sub LoadOrders()

        Dim reader As SqlDataReader
        Try
            _v.ListView1.Items.Clear()
            Dim order As BillOrder = New BillOrder()
            reader = order.GetOrderList()
            While (reader.Read())
                _v.ListView1.Items.Add(New ListViewItem(
                            New String() {
                                        reader(0).ToString(),
                                        reader(1).ToString(),
                                        reader(2).ToString(),
                                        reader(3).ToString()
                            }))
            End While
        Catch ex As Exception
            Console.WriteLine("Error: LoadOrders TablePresenter " & ex.ToString)
        End Try

    End Sub

    'check if table is already reserved
    'check if table number exist in listview
    'highlite busy table row in list
    Function IsTableBusy(ByVal table As String) As Boolean
        Dim res As Boolean = False
        Dim j As Integer = 0
        Try
            While j < _v.ListView1.Items.Count
                If (_v.ListView1.Items(j).SubItems(1).Text = table) Then
                    res = True

                    BtnArray(table).Text = CStr(table) & Environment.NewLine & "------------" & Environment.NewLine & "Rs. " & _v.ListView1.Items(j).SubItems(3).Text

                    Exit While

                End If
                j += 1
            End While

        Catch ex As Exception
            Console.WriteLine(ex)
        End Try
        
        Return res
    End Function


    Shared Function IsBusy(ByVal table As String, ByVal lv As ListView) As Boolean

        Dim res As Boolean = False
        Dim j As Integer = 0
        While j < lv.Items.Count
            If (lv.Items(j).SubItems(1).Text = table) Then
                res = True

                lv.Items(j).Focused = True
                lv.Items(j).Selected = True
                lv.Items(j).BackColor = Color.AliceBlue


            Else
                lv.Items(j).Focused = False
                lv.Items(j).Selected = False
                lv.Items(j).BackColor = Color.White
            End If
            
            j += 1
        End While
        Return res
    End Function


    'read select rows data from listview
    'to send to pay bill
    Private Sub ReadDataFromSelection()
        Dim billNo, tableNo, waiter, total As String
        If _v.ListView1.SelectedItems.Count > 0 Then
            Dim data As ListView.SelectedListViewItemCollection = _v.ListView1.SelectedItems
            Dim item As ListViewItem
            For Each item In data
                billNo = item.SubItems(0).Text
                tableNo = item.SubItems(1).Text
                waiter = item.SubItems(2).Text
                total = item.SubItems(3).Text

            Next
        End If
    End Sub

    Public Function ReadBillIdFromSelection() As String
        Return _v.ListView1.SelectedItems(0).Text()
    End Function
    Private Function ReadTableFromSelection() As String
        Return _v.ListView1.SelectedItems.Item(0).SubItems(1).Text()
    End Function

    Private Function ReadWaiterFromSelection() As String
        Return _v.ListView1.SelectedItems.Item(0).SubItems(2).Text()
    End Function

    ''' <summary>Update existing Bill Order. Read Bill ID, WaiterName, TableNo from selected row in List view and show Order Form</summary>
    ''' Update Bill Order
    ''' <exclude />
    Public Sub UpdateBill()
        Dim o As OrderForm = New OrderForm(_v)
        o.bill.WaiterName = ReadWaiterFromSelection()

        o.TblNo = ReadTableFromSelection()
        o.BillID = ReadBillIdFromSelection() 'send billID to orderForm attach with billorder model        
        o.Show()
    End Sub
End Class
