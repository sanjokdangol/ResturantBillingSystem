﻿Public Class MainForm
    Private randomizer As New Random
    Private addend1 As Integer
    Private addend2 As Integer
    Private timeLeft As Integer
    Private Sub ProductsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProductsToolStripMenuItem.Click
        Dim frm As ProductForm = New ProductForm()
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Private Sub CategoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CategoryToolStripMenuItem.Click
        Dim frm As CategoryForm = New CategoryForm()
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Private Sub UsersToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsersToolStripMenuItem.Click
        Dim frm As UsersForm = New UsersForm()
        frm.MdiParent = Me
        frm.Show()
    End Sub


    Private Sub WaiterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WaiterToolStripMenuItem.Click
        Dim frm As WaiterForm = New WaiterForm()
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If User.IsAdmin = False Then
            MasterToolStripMenuItem.Visible = False
            ReportsToolStripMenuItem.Visible = False

        End If
        ToolStripStatusLabel2.Text = User.CurrentUser
        Dim frm As TableForm = New TableForm()
        frm.MdiParent = Me
        frm.ClientSize = New Size(2000, 800)
        Me.WindowState = FormWindowState.Maximized

        frm.Dock = DockStyle.Fill
        frm.Show()
        LoginForm.Hide()
    End Sub

    Private Sub DailySalesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DailySalesToolStripMenuItem.Click
        Dim frm As DailySalesReportForm = New DailySalesReportForm()
        frm.Show()
    End Sub

    Private Sub DailyTransactionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DailyTransactionToolStripMenuItem.Click
        Dim frm As OverAllTransactionReportForm = New OverAllTransactionReportForm()
        'frm.MdiParent = Me
        'frm.ClientSize = New Size(2000, 800)
        'Me.WindowState = FormWindowState.Maximized
        'frm.FormBorderStyle = FormBorderStyle.None
        'frm.Dock = DockStyle.Fill
        frm.Show()
    End Sub

    Private Sub ViewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ViewToolStripMenuItem.Click
        Dim frm As TableForm = New TableForm()
        frm.MdiParent = Me
        frm.ClientSize = New Size(2000, 800)
        Me.WindowState = FormWindowState.Maximized
        frm.FormBorderStyle = FormBorderStyle.None
        frm.Dock = DockStyle.Fill
        frm.Show()
    End Sub

    Private Sub PaidBillToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PaidBillToolStripMenuItem.Click
        Dim frm As PaidBillForm = New PaidBillForm()
        frm.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub UnPaidBillsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnPaidBillsToolStripMenuItem.Click
        Dim frm As CreditSales = New CreditSales()
        frm.MdiParent = Me
        frm.ClientSize = New Size(2000, 800)
        Me.WindowState = FormWindowState.Maximized
        frm.FormBorderStyle = FormBorderStyle.None
        frm.Dock = DockStyle.Fill
        frm.Show()
    End Sub

    

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        ToolStripStatusLabel1.Text = System.DateTime.Now.ToString()
    End Sub


    Private Sub CalculatorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CalculatorToolStripMenuItem.Click
        System.Diagnostics.Process.Start("Calc.exe")
    End Sub

    Private Sub NotePadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NotePadToolStripMenuItem.Click
        System.Diagnostics.Process.Start("Notepad.exe")
    End Sub

    Private Sub WordPadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WordPadToolStripMenuItem.Click
        System.Diagnostics.Process.Start("WordPad.exe")
    End Sub

    Private Sub TaskManagerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TaskManagerToolStripMenuItem.Click
        System.Diagnostics.Process.Start("TaskMgr.exe")
    End Sub

    Private Sub CancelledOrdersToolStripMenuItem_Click(sender As Object, e As EventArgs)
        'Dim frm As CancelledOrdersForm = New CancelledOrdersForm()
        'frm.MdiParent = Me
        'frm.ClientSize = New Size(2000, 800)
        'Me.WindowState = FormWindowState.Maximized
        'frm.FormBorderStyle = FormBorderStyle.None
        'frm.Dock = DockStyle.Fill
        'frm.Show()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        
    End Sub

    Private Sub SoftwareToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SoftwareToolStripMenuItem.Click

    End Sub

    Private Sub ComanyToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ComanyToolStripMenuItem.Click
        Dim frm As AboutForm = New AboutForm()
       
        frm.Show()
    End Sub
End Class
